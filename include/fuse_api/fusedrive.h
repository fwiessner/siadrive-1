//
// Created by sgraves on 6/10/17.
//

#ifndef SIADRIVE_SIAFUSEDRIVE_H
#define SIADRIVE_SIAFUSEDRIVE_H
#include <siacommon.h>

#include <utility>

NS_BEGIN(Sia)
NS_BEGIN(Api)
NS_BEGIN(FUSE)
class FuseDriveException :
  std::exception {
  public:
    explicit FuseDriveException(std::string message) :
      _message(std::move(message)) {
    }

  private:
    const std::string _message;

  public:
    const char *what() const noexcept override {
      return _message.c_str();
    }
};

class CFuseDrive :
  public virtual ISiaDrive {
  public:
    CFuseDrive(std::shared_ptr<CSiaApi> siaApi, ITransferManager *transferManager);

  public:
    ~CFuseDrive() override;

  private:
    std::mutex _startStopMutex;

  public:
    void ClearCache() override;
    int GetMountResult() const;
    bool IsMounted() const override;
    void Mount(const std::vector<std::string>& args) override;
    void NotifyOnline() override;
    void NotifyOffline() override;
    void Unmount(const bool &clearCache = false) override;
    void Shutdown() override;
};
NS_END(3)
#endif //SIADRIVE_SIAFUSEDRIVE_H
