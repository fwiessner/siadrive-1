#pragma once
#include <siacommon.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)
class SIADRIVE_EXPORTABLE CSiaDriveConfig {
  public:
    CSiaDriveConfig(const bool &allowSave, std::shared_ptr<ISiaApiCommunicator> siaComm);
    CSiaDriveConfig(const bool &allowSave, const SString &filePath, std::shared_ptr<ISiaApiCommunicator> siaComm);
    explicit CSiaDriveConfig(const json &configDoc, std::shared_ptr<ISiaApiCommunicator> siaComm);

  public:
    ~CSiaDriveConfig();

  private:
    const bool _allowSave;
    std::shared_ptr<ISiaApiCommunicator> _siaComm;
    json _configDocument;
    std::mutex _saveLock;

  public:
    Property(SString, FilePath, public, private)
    JProperty(std::string, Renter_TransferDbFilePath, public, public, _configDocument)
    JProperty(std::string, VFS_SQLiteDbFilePath, public, public, _configDocument)
    JProperty(std::uint16_t, DriveApiPrivatePort, public, public, _configDocument)
    JProperty(std::uint16_t, DriveApiPublicPort, public, public, _configDocument)
    JProperty(std::string, CacheFolder, public, public, _configDocument)
    JProperty(std::uint16_t, SiaApiPort, public, public, _configDocument)
    JProperty(std::uint16_t, SiaHostPort, public, public, _configDocument)
    JProperty(std::uint16_t, SiaRpcPort, public, public, _configDocument)
    JProperty(std::int8_t, MaxUploadCount, public, public, _configDocument)
    JProperty(std::string, HostNameOrIp, public, public, _configDocument)
    JProperty(bool, LockWalletOnExit, public, public, _configDocument)
    JProperty(bool, LaunchBundledSiad, public, public, _configDocument)
    JProperty(bool, StopBundledSiad, public, public, _configDocument)
    JProperty(std::string, EventLevel, public, public, _configDocument)
    JProperty(bool, StoreUnlockPassword, public, public, _configDocument)
    JProperty(std::string, LastMountLocation, public, public, _configDocument)
    JProperty(long, SiaCommTimeoutMs, public, public, _configDocument)
    JProperty(std::string, LogDirectory, public, public, _configDocument)
    JProperty(std::uint8_t, DownloadTimeoutSeconds, public, public, _configDocument)
    JProperty(bool, EnableDriveEvents, public, public, _configDocument)
    JProperty(double, MinimumRedundancy, public, public, _configDocument)
    JProperty(bool, EnableMaximumCacheSize, public, public, _configDocument)
    JProperty(std::uint64_t, MaximumCacheSize, public, public, _configDocument)
    JProperty(std::uint8_t, EvictionDelay, public, public, _configDocument)

  private:
    bool LoadDefaults();
    void Load();

  public:
    SiaHostConfig GetHostConfig() const;
    std::shared_ptr<ISiaApiCommunicator> GetSiaCommunicator() const;
    bool Save();
    SString ToString() const;
};
NS_END(2)