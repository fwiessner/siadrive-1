#ifndef _SQLITEFILESTORE_H
#define _SQLITEFILESTORE_H
#include <siacommon.h>
#include <SQLiteCpp/Database.h>
#include <SQLiteCpp/Exception.h>
#include <atomic>
#include <vfs/ivfsitem.h>
#include <vfs/itemlock.h>
#include <eventsystem.h>
#include <vfs/vfsevents.h>
#include <vfs/vfscommon.h>
#ifndef _WIN32
#include <sys/stat.h>
#endif

NS_BEGIN(Sia)
NS_BEGIN(Api)

#define VFS_SQLITE_DB_VERSION 1ull

template<typename Item>
class CSQLiteItemStore {
  public:
    typedef Item StorageItem;
    typedef typename Item::ItemHandle Handle;
    typedef typename Item::ItemAccessData AccessData;
    typedef typename Item::ItemOpenData OpenData;
    typedef CVFSItemInfo<typename Item::ItemAccessData> ItemInfo;
    
  private:
    typedef std::function<bool(const SString& path, const bool& directory, const AccessData& accessData, const OpenData& openData, const bool& created)> AllowAccessCallback;
    typedef std::function<bool(Item *item)> CloseCallback;
    typedef std::function<bool(Handle &handle)> ItemCloser;
    typedef std::function<bool(Item *item)> CacheRequester;
    typedef std::function<void(Item *item)> ModifiedCallback;
    typedef IVFSItem<Handle, AccessData, OpenData> VFSItem;
    typedef std::shared_ptr<VFSItem> VFSItemPtr;

  public:
    template<typename... Args>
    explicit CSQLiteItemStore(AllowAccessCallback allowAccessCallback, CloseCallback closeCallback, ModifiedCallback modifiedCallback, CacheRequester cacheRequester, Args &&... args) :
      _allowAccessCallback(allowAccessCallback),
      _closeCallback(closeCallback),
      _modifiedCallback(modifiedCallback),
      _cacheRequester(cacheRequester),
      _database(std::forward<Args>(args)...) {
#ifndef _WIN32
      chmod(_database.getFilename().c_str(), 0600l);
#endif
      CreateTempOpenItemTable();
    }

  private:
    AllowAccessCallback _allowAccessCallback;
    CloseCallback _closeCallback;
    ModifiedCallback _modifiedCallback;
    CacheRequester _cacheRequester;
    SQLite::Database _database;

  private:
    VFSErrorCode CommonCreate(const SString &path, const SString &sourcePath, const bool &directory, AccessData accessData, const OpenData &openData, VFSItemPtr &item) {
      VFSErrorCode ret = VFSErrorCode::Success;
      try {
        if (_allowAccessCallback(path, directory, accessData, openData, true)) {
          InsertNewItem(path, sourcePath, directory, accessData);
          ret = CommonOpen(path, directory, openData, item, true);
          if (ret != VFSErrorCode::Success) {
            Remove(path, false);
          }
        } else {
          ret = VFSErrorCode::OSErrorCode;
        }
      } catch (const SQLite::Exception &e) {
        std::cout << e.what() << std::endl;
        CEventSystem::EventSystem.NotifyEvent(CreateVFSErrorEvent(__FUNCTION__, e.what()));
        ret = VFSErrorCode::Exception;
      }

      return ret;
    }

    VFSErrorCode CommonOpen(const SString &path, const bool &directory, const OpenData &openData, VFSItemPtr &item, const bool& created = false) {
      VFSErrorCode ret = VFSErrorCode::Success;
      try {
        AccessData accessData{};
        if (GetAccessData(path, accessData)) {
          bool allowAccess;
          if (created || _allowAccessCallback(path, directory, accessData, openData, false)) {
            std::uint64_t id;
            if (InsertOpenItem(path, directory, accessData, openData, id)) {
              item = std::make_shared<Item>(id, [this](Item *item) {
                this->RefreshCallback(item);
              }, [this](Item *item) -> bool {
                return this->FCloseCallback(item);
              }, [this](Item *item) {
                this->FModifiedCallback(item);
              }, [this](Item *item)->bool {
                return this->FCacheRequester(item);
              });
            } else {
              ret = directory ? VFSErrorCode::DirectoryNotFound : VFSErrorCode::ItemNotFound;
            }
          } else {
            ret = VFSErrorCode::OSErrorCode;
          }
        } else {
          ret = directory ? VFSErrorCode::DirectoryNotFound : VFSErrorCode::ItemNotFound;
        }
      } catch (const SQLite::Exception &e) {
        std::cout << e.what() << std::endl;
        CEventSystem::EventSystem.NotifyEvent(CreateVFSErrorEvent(__FUNCTION__, e.what()));
        ret = VFSErrorCode::Exception;
      }

      return ret;
    }

    void CreateItemTable() {
      SQLite::Statement createStmt(_database, SString() +
                                              "create table file_table (" +
                                              "id integer primary key autoincrement, " +
                                              "cached boolean not null default 0, " +
                                              "path text unique not null, " +
                                              "source text unique not null, " +
                                              "directory boolean not null, " +
                                              "status integer not null, " +
                                              "date integer not null, " +
                                              "handle integer not null default " + SString::FromInt64(VFSInvalidHandle) + ", " +
                                              "size unsigned big int not null default 0, " +
                                              "access_data blob not null);");
      createStmt.exec();
    }

    void CreateTempOpenItemTable() {
      SQLite::Statement createStmt(_database, SString() +
                                              "create temp table open_file_table (" +
                                              "id integer primary key autoincrement, " +
                                              "fid integer not null, " +
                                              "open_data blob not null);");
      createStmt.exec();
    }

    bool FCacheRequester(Item* item) {
      bool ret = _cacheRequester(item);
      if (ret) {
        UpdateDBCachedStatus(item);
      }

      return ret;
    }

    bool FCloseCallback(Item* item) {
      SQLite::Statement deleteStmt(_database, "delete from open_file_table where id=@id;");
      deleteStmt.bind("@id", (long long)item->GetId());
      deleteStmt.exec();

      bool closed = _closeCallback(item);
      if (closed) {
        UpdateDBStatusAndHandle(item);
      }

      return closed || (item->GetHandle() == VFSInvalidHandle);
    }

    void FModifiedCallback(Item* item) {
      _modifiedCallback(item);
      UpdateDBStatusAndItemSize(item);
    }

    std::uint64_t GetCurrentDbVersion() {
      std::uint64_t version = 0;

      SQLite::Statement query(_database, "PRAGMA user_version;");
      if (query.executeStep()) {
        version = static_cast<long long>(query.getColumn(0));
      }

      return version;
    }

    std::vector<OpenData> GetOpenDataList(const SString &path) {
      std::vector<OpenData> ret;

      return std::move(ret);
    }

    void InsertNewItem(const SString &path, const SString &sourcePath, const bool &directory, AccessData accessData) {
      const SString insertSql = "insert into file_table (path, source, directory, status, date, handle, access_data, cached) values (@path, @source, @directory, @status, @date, @handle, @access_data, @cached);";
      SQLite::Statement insertStmt(_database, insertSql);
      insertStmt.bind("@path", &path[0]);
      insertStmt.bind("@source", &sourcePath[0]);
      insertStmt.bind("@directory", directory);
      insertStmt.bind("@handle", (long long)VFSInvalidHandle);
      insertStmt.bind("@cached", false);
      insertStmt.bind("@status", (int)VFSItemStatus::Created);
      insertStmt.bind("@date", (long long)std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
      insertStmt.bind("@access_data", &accessData, sizeof(accessData));
      insertStmt.exec();
    }

    bool InsertOpenItem(const SString &path, const bool &directory, AccessData accessData, const OpenData &openData, std::uint64_t &id) {
      bool ret = false;
      static std::atomic<std::uint64_t> nextId(0);

      std::uint64_t fid;
      SQLite::Statement queryFid(_database, "select id, directory from file_table where path=@path LIMIT 1;");
      queryFid.bind("@path", &path[0]);
      if (queryFid.executeStep()) {
        fid = queryFid.getColumn(0).getInt64();
        if (directory == (bool) queryFid.getColumn(1).getInt()) {
          id = ++nextId;
          const SString insertSql = "insert into open_file_table (id, fid, open_data) values(@id, @fid, @open_data)";
          SQLite::Statement insertStmt(_database, insertSql);
          insertStmt.bind("@id", (long long) id);
          insertStmt.bind("@fid", (long long) fid);
          insertStmt.bind("@open_data", &openData, sizeof(OpenData));
          insertStmt.exec();
          ret = true;
        }
      }

      return ret;
    }

    void RefreshCallback(Item* item) {
      SQLite::Statement query(_database, "select * from open_file_table as o inner join file_table as f on o.fid = f.id where o.id=@id;");
      query.bind("@id", (long long)item->GetId());
      if (query.executeStep()) {
        item->SetAccessData(*(AccessData *) query.getColumn("access_data").getBlob());
        item->SetDate(query.getColumn("date").getInt64());
        item->SetSize(query.getColumn("size").getInt64());
        item->SetItemId(query.getColumn("file_table.id").getInt64());
        item->SetHandle((Handle) query.getColumn("handle").getInt64());
        item->SetIsCached((bool) query.getColumn("cached").getInt());
        item->SetIsDirectory((bool) query.getColumn("directory").getInt());
        item->SetOpenData(*(OpenData *) query.getColumn("open_data").getBlob());
        item->SetPath(query.getColumn("path").getText());
        item->SetSourcePath(query.getColumn("source").getText());
        item->SetStatus((VFSItemStatus) query.getColumn("status").getInt());
      }
    }

    void SetDatabaseVersion(const std::uint64_t& version) {
      const SString sql = "PRAGMA user_version=" + SString::FromUInt64(version) + ";";
      SQLite::Statement update(_database, sql);
      update.exec();
    }

    void UpdateDBCachedStatus(Item* item) {
      SQLite::Statement updateStmt(_database, "update file_table set cached=@cached where path=@path;");
      updateStmt.bind("@cached", item->IsCached());
      updateStmt.bind("@path", &item->GetPath()[0]);
      updateStmt.exec();
    }

    void UpdateDBStatusAndItemSize(Item* item) {
      SQLite::Statement updateStmt(_database, "update file_table set status=@status, size=@size where path=@path;");
      updateStmt.bind("@status", (int)item->GetStatus());
      updateStmt.bind("@path", &item->GetPath()[0]);
      updateStmt.bind("@size", (long long)item->GetSize());
      updateStmt.exec();
    }

    void UpdateDBStatusAndHandle(Item* item) {
      SQLite::Statement updateStmt(_database, "update file_table set status=@status, handle=@handle where path=@path;");
      updateStmt.bind("@status", (int)item->GetStatus());
      updateStmt.bind("@handle", (long long)item->GetHandle());
      updateStmt.bind("@path", &item->GetPath()[0]);
      updateStmt.exec();
    }

  public:
    inline VFSErrorCode Create(const SString &path, const SString &sourcePath, const bool &directory, const AccessData& accessData, const OpenData &openData, VFSItemPtr &item) {
      ItemLock lock(path);
      return CommonCreate(path, sourcePath, directory, accessData, openData, item);
    }

    bool GetAccessData(const SString& path, AccessData& accessData) {
      bool ret = false;
      SQLite::Statement query(_database, "select access_data from file_table where path=@path;");
      query.bind("@path", &path[0]);
      if (query.executeStep()) {
        accessData = *(AccessData *)query.getColumn(0).getBlob();
        ret = true;
      }

      return ret;
    }

    bool GetItemInfo(const SString &path, std::shared_ptr<ItemInfo>& itemInfo) {
      bool ret = false;
      SQLite::Statement query(_database, "select * from file_table where path=@path;");
      query.bind("@path", &path[0]);
      if (query.executeStep()) {
        itemInfo.reset(new ItemInfo(query.getColumn("id").getInt64(),
          query.getColumn("path").getString(),
          query.getColumn("source").getString(),
          (VFSItemStatus)query.getColumn("status").getInt(),
          (bool)query.getColumn("cached").getInt(),
          *(AccessData*)query.getColumn("access_data").getBlob(),
          (bool) query.getColumn("directory").getInt(),
          query.getColumn("date").getInt64(),
          query.getColumn("size").getInt64()));
        ret = true;
      }

      return ret;
    }

    std::uint32_t GetOpenCount(const SString &path) {
      std::uint32_t ret = 0;
      SQLite::Statement query(_database, "select COUNT(o.id) from open_file_table as o inner join file_table as f on o.fid = f.id where f.path=@path;");
      query.bind("@path", &path[0]);
      if (query.executeStep()) {
        ret = query.getColumn(0).getUInt();
      }

      return ret;
    }

    bool GetOpenItem(const std::uint64_t& id, VFSItemPtr& item) {

    }

    inline bool GetUpgradeRequired() const {
      return GetCurrentDbVersion() != VFS_SQLITE_DB_VERSION;
    }

    inline bool IsDirectory(const SString& path) {
      bool ret = false;
      SQLite::Statement dirStmt(_database, "select directory from file_table where path=@path;");
      if (dirStmt.executeStep()) {
        ret = (dirStmt.getColumn(0).getInt() != 0);
      }

      return ret;
    }

    inline VFSErrorCode Open(const SString &path, const bool &directory, const OpenData &openData, VFSItemPtr &item) {
      ItemLock lock(path);
      return CommonOpen(path, directory, openData, item);
    }

    VFSErrorCode OpenOrCreate(const SString &path, const SString &sourcePath, const bool &directory, AccessData accessData, const OpenData &openData, VFSItemPtr &item) {
      VFSErrorCode ret = VFSErrorCode::Success;
      ItemLock lock(path);
      try {
        if (GetOpenCount(path) == 0) {
          ret = CommonCreate(path, sourcePath, directory, accessData, openData, item);
        } else {
          ret = CommonOpen(path, directory, openData, item);
        }
      } catch (const SQLite::Exception &e) {
        std::cout << e.what() << std::endl;
        CEventSystem::EventSystem.NotifyEvent(CreateVFSErrorEvent(__FUNCTION__, e.what()));
        ret = VFSErrorCode::Exception;
      }

      return ret;
    }

    inline bool Remove(const SString &path) {
      return Remove(path, true);
    }

    bool Remove(const SString &path, const bool &lockRequired) {
      bool ret = false;
      std::unique_ptr<ItemLock> itemLock;
      if (lockRequired) {
        itemLock = std::make_unique<ItemLock>(path);
      }

      if (GetOpenCount(path) == 0) {
        SQLite::Statement deleteStmt(_database, "delete from file_table where path=@path;");
        deleteStmt.bind("@path", &path[0]);
        deleteStmt.exec();
        ret = true;
      }

      return ret;
    }

    bool SetAccessData(const SString& path, const AccessData& accessData) {
      ItemLock itemLock(path);
      try {

      } catch (const SQLite::Exception& e) {
        CEventSystem::EventSystem.NotifyEvent(CreateVFSErrorEvent(__FUNCTION__, e.what()));
      }
    }

    void Upgrade() {
      auto curVersion = GetCurrentDbVersion();
      if (curVersion != VFS_SQLITE_DB_VERSION) {
        const auto totalOps = VFS_SQLITE_DB_VERSION - curVersion;
        auto remainOps = totalOps;
        CEventSystem::EventSystem.NotifyEvent(CreateUpgradeProgressEvent(totalOps, remainOps));
        while (curVersion != VFS_SQLITE_DB_VERSION) {
          switch (curVersion) {
          case 0: {
            // Get Sia item list
            CEventSystem::EventSystem.NotifyEvent(CreateCurrentUpgradeProgressEvent(1, 1));
            CreateItemTable();
            CEventSystem::EventSystem.NotifyEvent(CreateCurrentUpgradeProgressEvent(1, 0));
          }
            break;
          }

          CEventSystem::EventSystem.NotifyEvent(CreateUpgradeProgressEvent(totalOps, --remainOps));
          SetDatabaseVersion(++curVersion);
        }
      }
    }
};

NS_END(2)
#endif //_SQLITEFILESTORE_H
