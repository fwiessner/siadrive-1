//
// Created by sgraves on 10/31/17.
//

#ifndef SIADRIVE_VFSCOMMON_H
#define SIADRIVE_VFSCOMMON_H
#include <siacommon.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)

#ifdef _WIN32
#define OSHandle HANDLE
#define VFSInvalidHandle INVALID_HANDLE_VALUE
#else
#define OSHandle int
#define VFSInvalidHandle -1ll
#endif

enum class VFSErrorCode {
    Success,
    AccessDenied,
    DirectoryNotFound,
    ItemNotFound,
    Exception,
    FileNotFound,
    OSErrorCode
};

enum class VFSItemStatus {
    Created,
    Modified
};

template <typename AccessData>
class CVFSItemInfo {
  public:
    explicit CVFSItemInfo(const std::uint64_t& iid, const SString& path, const SString& sourcePath, const VFSItemStatus& status,
                          const bool& isCached, const AccessData& accessData, const bool& isDirectory,
                          const std::uint64_t& date, const std::int64_t& fileSize) :
      _iid(iid),
      _path(path),
      _sourcePath(sourcePath),
      _status(status),
      _isCached(isCached),
      _accessData(accessData),
      _isDirectory(isDirectory),
      _date(date),
      _fileSize(fileSize) {

    }

  private:
    const std::uint64_t _iid;
    const SString _path;
    const SString _sourcePath;
    const VFSItemStatus _status;
    const bool _isCached;
    const AccessData _accessData;
    const bool _isDirectory;
    const std::uint64_t _date;
    const std::int64_t _fileSize;

  public:
    inline AccessData GetAccessData() const {
      return _accessData;
    }

    inline std::uint64_t GetDate() const {
      return _date;
    }

    inline std::uint64_t GetItemId() const {
      return _iid;
    }

    inline SString GetPath() const {
      return _path;
    }

    inline std::int64_t GetSize() const {
      return _fileSize;
    }

    inline SString GetSourcePath() const {
      return _sourcePath;
    }

    inline VFSItemStatus GetStatus() const {
      return _status;
    }

    inline bool IsCached() const {
      return _isCached;
    }

    inline bool IsDirectory() const {
      return _isDirectory;
    }
};

NS_END(2)
#endif //SIADRIVE_VFSCOMMON_H
