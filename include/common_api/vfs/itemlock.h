#ifndef SIADRIVE_FILELOCK_H
#define SIADRIVE_FILELOCK_H

#include <siacommon.h>
#ifdef _WIN32
#else
#include <sharedmutex.h>
#include <sys/mman.h>
#endif


NS_BEGIN(Sia)
NS_BEGIN(Api)
#ifdef _WIN32
#else
class ItemLock {
  public:
    NON_COPYABLE_ASSIGNABLE(ItemLock);

  public:
    explicit ItemLock(const SString &path) {
      _mutex = shared_mutex_init(&(SString("/") + GenerateSha256(path))[0]);
      //shm_unlink(_mutex.name);
      pthread_mutex_lock(_mutex.ptr);
    }

    ~ItemLock() {
      pthread_mutex_unlock(_mutex.ptr);
      shared_mutex_close(_mutex);
    }

  private:
    shared_mutex_t _mutex;
};
#endif
NS_END(2)
#endif //SIADRIVE_FILELOCK_H
