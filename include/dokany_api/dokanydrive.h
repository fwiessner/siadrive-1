#ifndef _SIADOKANDRIVE_H
#define _SIADOKANDRIVE_H

#ifdef _WIN32
#define WIN32_NO_STATUS

// Import or export functions and/or classes
#ifdef SIADRIVE_DOKAN_EXPORT_SYMBOLS
#define SIADRIVE_DOKAN_EXPORTABLE __declspec(dllexport)
#else
#define SIADRIVE_DOKAN_EXPORTABLE __declspec(dllimport)
#endif //SIADRIVE_DOKAN_EXPORT_SYMBOLS
#else
#define SIADRIVE_DOKAN_EXPORTABLE
#endif
#include <siacommon.h>
#include <siaapi.h>
#include <mutex>
#include <eventsystem.h>
#include <uploadmanager.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)
NS_BEGIN(Dokan)
class SIADRIVE_DOKAN_EXPORTABLE CDokanyDriveException :
  std::exception {
  public:
    CDokanyDriveException(char *message) :
      std::exception(message) {
    }
};

class SIADRIVE_DOKAN_EXPORTABLE CDokanyDrive :
  public virtual ISiaDrive {
  public:
    // throws SiaDokenDriveException
    CDokanyDrive(std::shared_ptr<CSiaApi> siaApi, IUploadManager *uploadManager);

  public:
    virtual ~CDokanyDrive();

  private:
    std::mutex _startStopMutex;

  public:
    virtual void ClearCache() override;
    virtual bool IsMounted() const override;
    virtual void Mount(const SString &location) override;
    virtual void NotifyOnline() override;
    virtual void NotifyOffline() override;
    virtual void Unmount(const bool &clearCache = false) override;
    virtual void Shutdown() override;
};
NS_END(3)
#endif //_SIADOKANDRIVE_H