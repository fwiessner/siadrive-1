#ifndef _PRIVATELISTENER_H
#define _PRIVATELISTENER_H
#include <siacommon.h>
#include <listenerbase.h>
#include <jsonrp.hpp>

using namespace web::http;

NS_BEGIN(Sia)
NS_BEGIN(Api)
class CSiaApi;
class CSiaDriveConfig;
class CSiaTransferManager;

class CPrivateListener :
  public CListenerBase {
  public:
    explicit CPrivateListener(std::shared_ptr<CSiaApi> siaApi);

  public:
    ~CPrivateListener() override;

  private:
    std::unique_ptr<CSiaTransferManager> _transferManager;
    std::unique_ptr<ISiaDrive> _siaDrive;

  private:
    void HandleGetMethod(http_request request, std::unique_ptr<jsonrpcpp::Response>& response);
    void HandlePostMethod(http_request request, std::unique_ptr<jsonrpcpp::Response>& response);

  protected:
    void ConfigureListener(http_listener* listener) override;
};
NS_END(2)

#endif //_PRIVATELISTENER_H
