#ifdef __linux__
#ifdef ENABLE_FUSE_TESTS
#if 0
#include "unittestcommon.h"
#include "siafuse_fixture.h"
#include <sys/sendfile.h>
#include <fcntl.h>
#include <sys/file.h>

using namespace std::literals::chrono_literals;

int OSCopyFile(const char *source, const char *destination) {
  int input, output;
  if ((input = open(source, O_RDONLY)) == -1) {
    return -1;
  }
  if ((output = open(destination, O_RDWR | O_CREAT, 0644)) == -1) {
    close(input);
    return -1;
  }

  off_t bytesCopied = 0;
  struct stat fileinfo = {0};
  fstat(input, &fileinfo);
  int result = sendfile(output, input, &bytesCopied, fileinfo.st_size);

  close(input);
  close(output);

  return result;
}

TEST_F(SiaFuseTest, IsNotMountedByDefault) {
  EXPECT_FALSE(fuseDrive.get()->IsMounted());
}

TEST_F(SiaFuseTest, MountAndUnmount) {
  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());
  EXPECT_TRUE(fuseDrive.get()->IsMounted());

  const auto expected = "mount | grep \"" + FilePath(siaApi->GetSiaDriveConfig()->GetLastMountLocation())
    .MakeAbsolute() + "\" >/dev/null";
  EXPECT_TRUE(system(&expected[0]) == 0);

  fuseDrive.get()->Unmount();
  EXPECT_FALSE(fuseDrive.get()->IsMounted());
  EXPECT_EQ(0, fuseDrive.get()->GetMountResult());
}

TEST_F(SiaFuseTest, UnmountOnDestructor) {
  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());

  fuseDrive.reset();

  const auto expected2 = "mount | grep \"" + FilePath(siaApi->GetSiaDriveConfig()->GetLastMountLocation())
    .MakeAbsolute() + "\" >/dev/null";
  ASSERT_TRUE(system(&expected2[0]) != 0);
}

TEST_F(SiaFuseTest, CopyNewFile) {
  EXPECT_CALL(*mockComm, Post(_, _, _))
    .WillOnce(Return(SiaCommError(SiaCommErrorCode::HttpError, "no file known")));
  EXPECT_CALL(*mockComm, Get(_, _))
    .WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());
  EXPECT_TRUE(fuseDrive.get()->IsMounted());

  FilePath src(__FILE__);
  FilePath dst(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), FilePath(src).StripToFileName());

  OSCopyFile(&src[0], &dst[0]);
  EXPECT_TRUE(dst.IsFile());
  EXPECT_TRUE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), FilePath(src).StripToFileName()).IsFile());

  fuseDrive.get()->Shutdown();
  EXPECT_FALSE(dst.IsFile());
  EXPECT_TRUE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), FilePath(src).StripToFileName()).IsFile());
}

TEST_F(SiaFuseTest, DeleteFile) {
  EXPECT_CALL(*mockComm, Post(_, _, _))
    .WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::HttpError, "no file known")));
  EXPECT_CALL(*mockComm, Get(_, _)).WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());

  FilePath src(__FILE__);
  FilePath dst(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), FilePath(src).StripToFileName());

  OSCopyFile(&src[0], &dst[0]);

  unlink(&dst[0]);
  EXPECT_FALSE(dst.IsFile());
  EXPECT_FALSE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), FilePath(src).StripToFileName()).IsFile());

  fuseDrive.get()->Shutdown();
  EXPECT_FALSE(dst.IsFile());
  EXPECT_FALSE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), FilePath(src).StripToFileName()).IsFile());
}

TEST_F(SiaFuseTest, RenameLocalFileNotInSiaAndAddToSia) {
  EXPECT_CALL(*mockComm, Post(_, _, _)).WillOnce(Return(SiaCommError(SiaCommErrorCode::HttpError, "no file known")))
    .WillOnce(Return(SiaCommError(SiaCommErrorCode::Success)));

  EXPECT_CALL(*mockComm, Get(_, _))
    .WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::NoResponse)));
  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());

  FilePath src(__FILE__);
  FilePath dst(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), FilePath(src).StripToFileName());

  OSCopyFile(&src[0], &dst[0]);

  SString n = dst;
  n += ".tmp";

  rename(&dst[0], &n[0]);
  EXPECT_FALSE(dst.IsFile());
  EXPECT_FALSE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), FilePath(src).StripToFileName()).IsFile());
  EXPECT_TRUE(FilePath(n).IsFile());
  EXPECT_TRUE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), (SString) FilePath(src)
    .StripToFileName() + ".tmp").IsFile());

  fuseDrive.get()->Shutdown();
  EXPECT_FALSE(dst.IsFile());
  EXPECT_FALSE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), FilePath(src).StripToFileName()).IsFile());
  EXPECT_FALSE(FilePath(n).IsFile());
  EXPECT_TRUE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), (SString) FilePath(src)
    .StripToFileName() + ".tmp").IsFile());

  FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), (SString) FilePath(src).StripToFileName() + ".tmp")
    .DeleteAsFile();
}

TEST_F(SiaFuseTest, CreateDirectory) {
  EXPECT_CALL(*mockComm, Get(_, _))
    .WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::NoResponse)));
  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());

  const FilePath dir(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), "test_dir");

  mkdir(&dir[0], S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  EXPECT_TRUE(dir.IsDirectory());
  EXPECT_TRUE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test_dir").IsDirectory());

  fuseDrive.get()->Shutdown();
  EXPECT_FALSE(dir.IsDirectory());
  EXPECT_TRUE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test_dir").IsDirectory());
  FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test_dir").DeleteAsDirectory();
}

TEST_F(SiaFuseTest, RemoveDirectory) {
  EXPECT_CALL(*mockComm, Get(_, _))
    .WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());

  const FilePath dir(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), "test_dir");

  mkdir(&dir[0], S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  EXPECT_TRUE(dir.IsDirectory());
  EXPECT_TRUE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test_dir").IsDirectory());

  rmdir(&dir[0]);
  EXPECT_FALSE(dir.IsDirectory());
  EXPECT_FALSE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test_dir").IsDirectory());

  fuseDrive.get()->Shutdown();
  EXPECT_FALSE(dir.IsDirectory());
  EXPECT_FALSE(FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test_dir").IsDirectory());
}

TEST_F(SiaFuseTest, Access) {
  EXPECT_CALL(*mockComm, Post(_, _, _)).WillOnce(Return(SiaCommError(SiaCommErrorCode::HttpError, "no file known")));
  EXPECT_CALL(*mockComm, Get(_, _))
    .WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::NoResponse)));
  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());
  EXPECT_TRUE(fuseDrive.get()->IsMounted());

  FilePath src(__FILE__);
  FilePath dst(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), FilePath(src).StripToFileName());

  OSCopyFile(&src[0], &dst[0]);
  EXPECT_TRUE(dst.IsFile());

  EXPECT_EQ(0, access(&dst[0], F_OK));

  fuseDrive.get()->Shutdown();
  FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), FilePath(__FILE__).StripToFileName()).DeleteAsFile();
}

TEST_F(SiaFuseTest, Chmod) {
  EXPECT_CALL(*mockComm, Post(_, _, _)).WillOnce(Return(SiaCommError(SiaCommErrorCode::HttpError, "no file known")));
  EXPECT_CALL(*mockComm, Get(_, _))
    .WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());
  EXPECT_TRUE(fuseDrive.get()->IsMounted());

  FilePath src(__FILE__);
  FilePath dst(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), FilePath(src).StripToFileName());

  OSCopyFile(&src[0], &dst[0]);
  EXPECT_TRUE(dst.IsFile());

  EXPECT_EQ(0, chmod(&dst[0], S_IXUSR | S_IXGRP | S_IXOTH));
  auto cache = FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), FilePath(__FILE__).StripToFileName());

  struct stat st{};
  stat(&cache[0], &st);
  EXPECT_EQ((0777u & (S_IXUSR | S_IXGRP | S_IXOTH)), (0777u & st.st_mode));

  fuseDrive.get()->Shutdown();
  FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), FilePath(__FILE__).StripToFileName()).DeleteAsFile();
}

TEST_F(SiaFuseTest, Fallocate) {
  EXPECT_CALL(*mockComm, Post(_, _, _)).WillOnce(Return(SiaCommError(SiaCommErrorCode::HttpError, "no file known")));
  EXPECT_CALL(*mockComm, Get(_, _))
    .WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());
  EXPECT_TRUE(fuseDrive.get()->IsMounted());

  FilePath src(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), "test.fallocate");

  auto fd = open(&src[0], O_RDWR | O_CREAT, 0666u);
  EXPECT_GE(fd, 0);
  if (fd >= 0) {

    EXPECT_EQ(0, fallocate(fd, 0, 0, 100));
    close(fd);

    EXPECT_TRUE(src.IsFile());
    EXPECT_EQ(100, FilePath::FileSize(src));
  }

  fuseDrive.get()->Shutdown();
  src = FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test.fallocate");
  src.DeleteAsFile();
}

TEST_F(SiaFuseTest, Write) {
  EXPECT_CALL(*mockComm, Post(_, _, _)).WillOnce(Return(SiaCommError(SiaCommErrorCode::HttpError, "no file known")));
  EXPECT_CALL(*mockComm, Get(_, _))
    .WillRepeatedly(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());
  EXPECT_TRUE(fuseDrive.get()->IsMounted());

  FilePath src(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), "test.write");

  auto fd = open(&src[0], O_RDWR | O_CREAT, 0666u);
  EXPECT_GE(fd, 0);
  if (fd >= 0) {
    std::uint8_t b = 1;
    EXPECT_EQ(1, write(fd, &b, 1));
    close(fd);

    EXPECT_TRUE(src.IsFile());
    EXPECT_EQ(1, FilePath::FileSize(src));
  }

  fuseDrive.get()->Shutdown();
  src = FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test.write");
  src.DeleteAsFile();
}

#ifndef __APPLE__

TEST_F(SiaFuseTest, FlockAndUnlock) {
  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());
  EXPECT_TRUE(fuseDrive.get()->IsMounted());

  FilePath src(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), "test.flock");

  auto fd = open(&src[0], O_RDWR | O_CREAT, 0666u);
  EXPECT_GE(fd, 0);
  if (fd >= 0) {
    EXPECT_EQ(0, flock(fd, LOCK_EX));
    EXPECT_EQ(0, flock(fd, LOCK_UN));
    close(fd);
  }

  fuseDrive.get()->Shutdown();
  src = FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test.flock");
  src.DeleteAsFile();
}

#endif

TEST_F(SiaFuseTest, Fsync) {
  fuseDrive.get()->Mount(siaApi->GetSiaDriveConfig()->GetLastMountLocation());
  EXPECT_TRUE(fuseDrive.get()->IsMounted());

  FilePath src(siaApi->GetSiaDriveConfig()->GetLastMountLocation(), "test.fsync");
  auto fd = open(&src[0], O_RDWR | O_CREAT, 0666u);
  EXPECT_GE(fd, 0);
  if (fd >= 0) {
    EXPECT_EQ(0, fsync(fd));
    close(fd);
  }

  fuseDrive.get()->Shutdown();
  src = FilePath(siaApi->GetSiaDriveConfig()->GetCacheFolder(), "test.fsync");
  src.DeleteAsFile();
}

#endif
#endif
#endif
