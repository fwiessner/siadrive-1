#include <dokanydrive.h>
#include <filesystem>
#include <dokan.h>
#include <filepath.h>
#include <siadriveconfig.h>
#include <siacommon.h>
#include <siadrivebase.h>
#include <atomic>

using namespace Sia::Api;
using namespace Dokan;
using namespace std::chrono_literals;

typedef struct _DokanFileConfig {
  ACCESS_MASK DesiredAccess;
  ULONG ShareMode;
  std::shared_ptr<SECURITY_ATTRIBUTES> SecurityAttrib;
  ULONG CreateDisp;
  DWORD Flags;
} DokanFileConfig;

BEGIN_EVENT(DokanyEvent)
  public:
    DokanyEvent(const SString& functionName, const HRESULT &result, const SString &siaPath, const SString& filePath) :
      CEvent(EventLevel::Debug),
      _functionName(functionName),
      _result(result),
      _siaPath(siaPath),
      _filePath(filePath) {
    }

  private:
    DokanyEvent()  :
      CEvent(EventLevel::Debug) {
    }

  public:
    virtual ~DokanyEvent() {
    }

  private:
    SString _functionName;
    int _result;
    SString _siaPath;
    SString _filePath;

  protected:
    virtual void LoadData(const json& j) override {
      _functionName = j["function_name"].get<std::string>();
      _result = j["result"].get<int>();
      _siaPath = j["sia_path"].get<std::string>();
      _filePath = j["file_path"].get<std::string>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new DokanyEvent(_functionName, _result, _siaPath, _filePath));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|FUNC|" + _functionName + "|RES|" + SString::FromUInt32(_result) + "|SP|" + _siaPath + "|FP|" + _filePath;
    }

    virtual json GetEventJson() const override {
      return {{"event",         GetEventName()},
              {"function_name", _functionName},
              {"result",        _result},
              {"sia_path",      _siaPath } ,
              {"file_path",     _filePath } };
    }
END_EVENT(DokanyEvent);

CEventPtr CreateDokanyEvent(const SString &functionName, const NTSTATUS &ret, const SString &siaPath, const SString& filePath) {
  return CEventPtr(new DokanyEvent(functionName, ret, siaPath, filePath));
}

#define DOKANY_EVENT(func, ret, siaPath, filePath) \
([&](const NTSTATUS& r, const SString& fname, const SString& sp, const SString fp)->NTSTATUS {\
  if (GetSiaDriveConfig().GetEnableDriveEvents() && ((EventLevelFromString(GetSiaDriveConfig().GetEventLevel()) >= EventLevel::Debug) || (r != 0))) {\
    CEventSystem::EventSystem.NotifyEvent(CreateDokanyEvent(fname, r, sp, fp));\
  }\
  return r;\
})(ret, func, siaPath, filePath);

// TODO Handle paths greater than MAX_PATH!!

class SIADRIVE_DOKAN_EXPORTABLE DokanyImpl {
  private:
    static DOKAN_OPERATIONS _dokanOps;
    static DOKAN_OPTIONS _dokanOptions;
    static std::unique_ptr<std::thread> _mountThread;
    static NTSTATUS _mountStatus;
    static SString _mountPoint;

  private:
    static inline bool CheckAccess(const ACCESS_MASK& access, const ACCESS_MASK& required) {
      return (access & required) || (access & GENERIC_ALL);
    }

    template<typename T>
    static void SetCachedFileTime(const SString &filePath, T *fd) {
      WIN32_FIND_DATA find = {0};
      HANDLE findHandle = ::FindFirstFile(&filePath[0], &find);
      if (findHandle != INVALID_HANDLE_VALUE) {
        fd->ftCreationTime = find.ftCreationTime;
        fd->ftLastAccessTime = find.ftLastAccessTime;
        fd->ftLastWriteTime = find.ftLastWriteTime;
        ::FindClose(findHandle);
      }
    }

    static BOOL AddRemovePrivilege(LPCSTR priv, BOOL add) {
      BOOL ret = FALSE;
      LUID luid;
      if (::LookupPrivilegeValue(0, priv, &luid) || (::GetLastError() == ERROR_SUCCESS)) {
        LUID_AND_ATTRIBUTES attr{};
        attr.Attributes = add ? SE_PRIVILEGE_ENABLED : SE_PRIVILEGE_REMOVED;
        attr.Luid = luid;

        TOKEN_PRIVILEGES priv{};
        priv.PrivilegeCount = 1;
        priv.Privileges[0] = attr;

        HANDLE token = nullptr;
        if (::OpenProcessToken(::GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &token) || (::GetLastError() == ERROR_SUCCESS)) {
          TOKEN_PRIVILEGES oldPriv;
          DWORD retSize;
          ::AdjustTokenPrivileges(token, FALSE, &priv, sizeof(TOKEN_PRIVILEGES), &oldPriv, &retSize);
          ret = (::GetLastError() == ERROR_SUCCESS);
          ::CloseHandle(token);
        }
      }
      return ret;
    }
      
    // Dokan callbacks
  private:
    static NTSTATUS DOKAN_CALLBACK Sia_ZwCreateFile(LPCWSTR fileName, PDOKAN_IO_SECURITY_CONTEXT securityContext, ACCESS_MASK desiredAccess, ULONG fileAttributes, ULONG shareAccess, ULONG createDisposition, ULONG createOptions, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      dokanFileInfo->Context = 0;

      SECURITY_ATTRIBUTES securityAttrib;
      securityAttrib.nLength = sizeof(securityAttrib);
      securityAttrib.lpSecurityDescriptor = securityContext->AccessState.SecurityDescriptor;
      securityAttrib.bInheritHandle = FALSE;

      DWORD fileAttributesAndFlags;
      DWORD creationDisposition;
      DokanMapKernelToUserCreateFileFlags(fileAttributes, createOptions, createDisposition, &fileAttributesAndFlags, &creationDisposition);

      ACCESS_MASK genericDesiredAccess = DokanMapStandardToGenericAccess(desiredAccess);
      NTSTATUS ret = STATUS_SUCCESS;
      // Probably not going to happen, but just in case
      if (FilePath(fileName).IsUNC()) {
        ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
      } else {
        // When filePath is a directory, needs to change the flag so that the file can
        // be opened.
        DWORD fileAttr = ::GetFileAttributes(&filePath[0]);
        if ((fileAttr != INVALID_FILE_ATTRIBUTES) && (fileAttr & FILE_ATTRIBUTE_DIRECTORY) && not(createOptions & FILE_NON_DIRECTORY_FILE)) {
          dokanFileInfo->IsDirectory = TRUE;
          if (desiredAccess & DELETE) {
            // Needed by FindFirstFile to see if directory is empty or not
            shareAccess |= FILE_SHARE_READ;
          }
        }

        DWORD openDisposition = creationDisposition;
        bool openAlwaysExists = false;
        if (dokanFileInfo->IsDirectory) {
          if (not filePath.IsDirectory() && GetFileTree()->DirectoryExists(siaPath)) {
            if (not CreateStubIfMissing(siaPath, filePath)) {
              ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
            }
          }
          if (ret == STATUS_SUCCESS) {
            if (((creationDisposition == OPEN_ALWAYS) || (creationDisposition == CREATE_ALWAYS)) &&
              (filePath.IsFile() || GetFileTree()->FileExists(siaPath))) {
              ret = STATUS_OBJECT_NAME_COLLISION;
            }
            else {
              if (creationDisposition == CREATE_NEW) {
                if (filePath.IsDirectory()) {
                  ret = DokanNtStatusFromWin32(ERROR_FILE_EXISTS);
                }
              }
              else if (creationDisposition == OPEN_ALWAYS) {
                openAlwaysExists = filePath.IsDirectory();
              }
              else if (createDisposition == OPEN_EXISTING) {
                if (not filePath.IsDirectory()) {
                  ret = DokanNtStatusFromWin32(ERROR_FILE_NOT_FOUND);
                }
              }

              if (ret == STATUS_SUCCESS) {
                //Check first if we're trying to open a file as a directory.
                if (fileAttr != INVALID_FILE_ATTRIBUTES && not(fileAttr & FILE_ATTRIBUTE_DIRECTORY) && (createOptions & FILE_DIRECTORY_FILE)) {
                  ret = STATUS_NOT_A_DIRECTORY;
                }
                else {
                  // FILE_FLAG_BACKUP_SEMANTICS is required for opening directory handles
                  fileAttributesAndFlags |= (FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_POSIX_SEMANTICS);
                }
              }
            }
          }
        } else // File (cache and/or Sia operation)
        {
          if (siaPath.Length()) {
            if (not filePath.IsFile() && GetFileTree()->FileExists(siaPath)) {
              if (not CreateStubIfMissing(siaPath, filePath)) {
                ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
              }
            }

            if (ret == STATUS_SUCCESS) {
              if (((creationDisposition == OPEN_ALWAYS) || (creationDisposition == CREATE_ALWAYS)) &&
                (filePath.IsDirectory() || GetFileTree()->DirectoryExists(siaPath))) {
                ret = STATUS_OBJECT_NAME_COLLISION;
              }
              else {
                bool fileExists = filePath.IsFile();
                bool siaExists = GetFileTree()->FileExists(siaPath);
                bool exists = siaExists || fileExists;
 
                switch (creationDisposition) {
                case CREATE_ALWAYS: {
                  if (exists) {
                    if (RemoveFile(siaPath, filePath)) {
                      openDisposition = OPEN_ALWAYS;
                    }
                    else if (ApiSuccess(GetUploadManager().Remove(siaPath)) &&
                      filePath.DeleteAsFile()) {
                      openDisposition = OPEN_ALWAYS;
                    }
                    else {
                      ret = DokanNtStatusFromWin32(::GetLastError());
                    }
                  }
                  else {
                    ret = DokanNtStatusFromWin32(ERROR_FILE_NOT_FOUND);
                  }
                }
                break;

                case CREATE_NEW: {
                  if (exists) {
                    ret = DokanNtStatusFromWin32(ERROR_FILE_EXISTS);
                  }
                }
                break;

                case OPEN_ALWAYS: {
                  openAlwaysExists = exists;
                }
                break;
  
                case OPEN_EXISTING: {
                  if (not exists) {
                    ret = DokanNtStatusFromWin32(ERROR_FILE_NOT_FOUND);
                  }
                }
                break;

                case TRUNCATE_EXISTING: {
                  if (exists) {
                    if (not DriveTools::IsStubFile(filePath) && TruncateAndRemoveFull(siaPath, filePath)) {
                      SetFileChanged(siaPath, filePath);
                      openDisposition = OPEN_EXISTING;
                    }
                    else {
                      RequiredHandleOperation(siaPath, filePath, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
                        LARGE_INTEGER distanceToMove{};
                        distanceToMove.QuadPart = 0;
                        if (::SetFilePointerEx(handle, distanceToMove, nullptr, FILE_BEGIN) &&
                          ::SetEndOfFile(handle)) {
                          openDisposition = OPEN_EXISTING;
                          fileChanged = true;
                        }
                        else {
                          ret = DokanNtStatusFromWin32(::GetLastError());
                        }
                      }, DriveTools::IsStubFile(filePath));
                    }
                  }
                  else {
                    ret = DokanNtStatusFromWin32(ERROR_FILE_NOT_FOUND);
                  }
                }
                break;

                default:
                  // Nothing to do
                  break;
                }
              }
            }
          } else {
            ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
          }
        }

        if (ret == STATUS_SUCCESS) {
          DokanFileConfig dfc{};
          dfc.CreateDisp = openDisposition;
          dfc.DesiredAccess = genericDesiredAccess;
          dfc.SecurityAttrib = std::make_shared<SECURITY_ATTRIBUTES>(securityAttrib);
          dfc.ShareMode = shareAccess;
          dfc.Flags = fileAttributesAndFlags;

          FileConfig fileConfig;
          fileConfig.IsDirectory = dokanFileInfo->IsDirectory;
          fileConfig.Key = dokanFileInfo->Context = CreateKey();
          fileConfig.PlatformData = dfc;

          if (AddOpenFile(siaPath, filePath, fileConfig)) {
            if ((createDisposition == OPEN_ALWAYS) && openAlwaysExists) {
              ::SetLastError(ERROR_ALREADY_EXISTS);
            }
          }
          else {
            dokanFileInfo->Context = 0;
            ret = DokanNtStatusFromWin32(::GetLastError());
          }
        }
      }
      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static inline void ConvertFindData(const WIN32_FIND_DATA& fd, WIN32_FIND_DATAW& fdw) {
      wcscpy(fdw.cAlternateFileName, SString::FromUtf8(fd.cAlternateFileName).c_str());
      wcscpy(fdw.cFileName, SString::FromUtf8(fd.cFileName).c_str());
      fdw.dwFileAttributes = fd.dwFileAttributes;
      fdw.dwReserved0 = fd.dwReserved0;
      fdw.dwReserved1 = fd.dwReserved1;
      fdw.ftCreationTime = fd.ftCreationTime;
      fdw.ftLastAccessTime = fd.ftLastAccessTime;
      fdw.ftLastWriteTime = fd.ftLastWriteTime;
      fdw.nFileSizeHigh = fd.nFileSizeHigh;
      fdw.nFileSizeLow = fd.nFileSizeLow;
    }

    static NTSTATUS DOKAN_CALLBACK Sia_FindFiles(LPCWSTR fileName, PFillFindData fillFindData, PDOKAN_FILE_INFO dokanFileInfo) {
      NTSTATUS ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
      auto siaFileTree = GetFileTree();
      if (siaFileTree) {
        SString siaFileQuery = CSiaApi::FormatToSiaPath(fileName);
        SString siaRootPath = CSiaApi::FormatToSiaPath(fileName);
        SString siaDirQuery;
        FilePath findFile = GetCacheLocation();
        FilePath cachePath = GetCacheLocation();
        if (FilePath::DirSep == fileName) {
          siaFileQuery += "/*.*";
          siaDirQuery = "/";
          findFile.Append("*");
        } else {
          cachePath.Append(fileName);
          findFile.Append(fileName);
          if (dokanFileInfo->IsDirectory) {
            siaDirQuery = siaFileQuery;
            siaFileQuery += "/*.*";
            findFile.Append("*");
          }
        }
        WIN32_FIND_DATA findData = {0};
        HANDLE findHandle = ::FindFirstFile(&findFile[0], &findData);
        if (findHandle == INVALID_HANDLE_VALUE) {
          ret = DokanNtStatusFromWin32(::GetLastError());
        } else {
          // Find local files
          std::unordered_map<SString, uint8_t> dirs;
          std::unordered_map<SString, uint8_t> files;
          do {
            if ((wcscmp(fileName, L"\\") != 0) || ((strcmp(findData.cFileName, ".") != 0) && (strcmp(findData.cFileName, "..") != 0))) {
              if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                dirs.insert({findData.cFileName,
                             0});
              } else {
                files.insert({findData.cFileName,
                              1});

                const FilePath filePath(cachePath, findData.cFileName);
                if (DriveTools::IsStubFile(filePath) && siaFileTree) {
                  auto siaPath = CSiaApi::FormatToSiaPath(FilePath(siaRootPath, findData.cFileName));
                  auto siaFile = siaFileTree->GetFile(siaPath);
                  if (siaFile) {
                    ULARGE_INTEGER li{};
                    li.QuadPart = siaFile->GetFileSize();
                    findData.nFileSizeHigh = li.HighPart;
                    findData.nFileSizeLow = li.LowPart;
                  }
                }
              }

              WIN32_FIND_DATAW fdw{};
              ConvertFindData(findData, fdw);
              fillFindData(&fdw, dokanFileInfo);
            }
          } while (::FindNextFile(findHandle, &findData) != 0);

          DWORD error = ::GetLastError();
          ::FindClose(findHandle);
          if (error == ERROR_NO_MORE_FILES) {
            // Find Sia directories
            if (not siaDirQuery.IsNullOrEmpty()) {
              auto dirList = siaFileTree->QueryDirectories(siaDirQuery);
              for (auto &dir : dirList) {
                if (dirs.find(dir) == dirs.end()) {
                  // Create cache sub-folder
                  FilePath subCachePath(cachePath, dir);
                  if (not subCachePath.IsDirectory()) {
                    subCachePath.MakeDirectory();
                  }
                  WIN32_FIND_DATAW fd = {0};
                  wcscpy(fd.cFileName, SString::FromUtf8(dir).c_str());
                  fd.dwFileAttributes = FILE_ATTRIBUTE_DIRECTORY;
                  
                  fillFindData(&fd, dokanFileInfo);
                }
              }
            }

            // Find Sia files
            auto fileList = siaFileTree->Query(siaFileQuery);
            for (auto &file :
              fileList) {
              FilePath fp = file->GetSiaPath();
              fp.StripToFileName();
              if (files.find(fp) == files.end()) {
                WIN32_FIND_DATA fd = {0};
                strcpy(fd.cFileName, &fp[0]);
                
                ULARGE_INTEGER li{};
                li.QuadPart = file->GetFileSize();
                fd.nFileSizeHigh = li.HighPart;
                fd.nFileSizeLow = li.LowPart;
                fd.dwFileAttributes = FILE_ATTRIBUTE_NORMAL;
                SetCachedFileTime(FilePath(GetCacheLocation(), file->GetSiaPath()), &fd);

                WIN32_FIND_DATAW fdw{};
                ConvertFindData(fd, fdw);
                fillFindData(&fdw, dokanFileInfo);
              }
            }
            ret = STATUS_SUCCESS;
          } else {
            ret = DokanNtStatusFromWin32(::GetLastError());
          }
        }
      }
      return DOKANY_EVENT(__FUNCTION__, ret, "", fileName);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_GetFileInformation(LPCWSTR fileName, LPBY_HANDLE_FILE_INFORMATION handleFileInfo, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);

      NTSTATUS ret = STATUS_SUCCESS;
      ExecuteHandleOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not (CheckAccess(platformData.DesiredAccess, FILE_READ_ATTRIBUTES) || 
          CheckAccess(platformData.DesiredAccess, GENERIC_READ) ||
          CheckAccess(platformData.DesiredAccess, FILE_READ_DATA) ||
          CheckAccess(platformData.DesiredAccess, SYNCHRONIZE))) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else {
          auto siaFile = GetFileTree()->GetFile(siaPath);
          if (siaFile && (DriveTools::IsStubFile(filePath) || (not filePath.IsFile() && not filePath.IsDirectory() && CreateStubIfMissing(filePath, siaPath)))) {
            ULARGE_INTEGER li{};
            li.QuadPart = siaFile->GetFileSize();
            if (::GetFileInformationByHandle(handle, handleFileInfo)) {
              handleFileInfo->nFileSizeHigh = li.HighPart;
              handleFileInfo->nFileSizeLow = li.LowPart;
              SetCachedFileTime(filePath, handleFileInfo);
            }
            else {
              ret = DokanNtStatusFromWin32(::GetLastError());
            }
          }
          else if (not ::GetFileInformationByHandle(handle, handleFileInfo)) {
            // fileName is a root directory
            // in this case, FindFirstFile can't get directory information
            if (wcscmp(fileName, L"\\") == 0) {
              handleFileInfo->dwFileAttributes = ::GetFileAttributes(&filePath[0]);
            }
            else {
              WIN32_FIND_DATA find = { 0 };
              HANDLE findHandle = ::FindFirstFile(&filePath[0], &find);
              if (findHandle == INVALID_HANDLE_VALUE) {
                ret = DokanNtStatusFromWin32(::GetLastError());
              }
              else {
                handleFileInfo->dwFileAttributes = find.dwFileAttributes;
                handleFileInfo->ftCreationTime = find.ftCreationTime;
                handleFileInfo->ftLastAccessTime = find.ftLastAccessTime;
                handleFileInfo->ftLastWriteTime = find.ftLastWriteTime;
                handleFileInfo->nFileSizeHigh = find.nFileSizeHigh;
                handleFileInfo->nFileSizeLow = find.nFileSizeLow;
                ::FindClose(findHandle);
              }
            }
          }
        }
      }, false);

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_Mounted(PDOKAN_FILE_INFO dokanFileInfo) {
      SetMounted(true);
      GetSiaApi().GetSiaDriveConfig()->SetLastMountLocation(_mountPoint);
      if (GetSiaApi().GetSiaDriveConfig()->GetLaunchFileMgrOnMount()) {
        ::ShellExecute(nullptr, "open", &_mountPoint[0], nullptr, nullptr, SW_SHOWDEFAULT);
      }
      CEventSystem::EventSystem.NotifyEvent(CreateMountedEvent(_mountPoint));
      return STATUS_SUCCESS;
    }

    static NTSTATUS DOKAN_CALLBACK Sia_Unmounted(PDOKAN_FILE_INFO dokanFileInfo) {
      SetMounted(false);
      CEventSystem::EventSystem.NotifyEvent(CreateUnMountedEvent(_mountPoint));
      return STATUS_SUCCESS;
    }

    static NTSTATUS DOKAN_CALLBACK Sia_GetDiskFreeSpace(PULONGLONG FreeBytesAvailable, PULONGLONG TotalNumberOfBytes, PULONGLONG TotalNumberOfFreeBytes, PDOKAN_FILE_INFO dokanFileInfo) {
      UNREFERENCED_PARAMETER(dokanFileInfo);
      SiaCurrency totalBytes;
      GetSiaApi().GetRenter().CalculateEstimatedStorage(GetSiaApi().GetRenter().GetFunds(), totalBytes);
      SiaCurrency totalUsed = GetSiaApi().GetRenter().GetTotalUsedBytes();
      *FreeBytesAvailable = (totalBytes == 0) ? 0 : (totalBytes - totalUsed).ToUInt();
      *TotalNumberOfFreeBytes = *TotalNumberOfBytes = totalBytes.ToUInt();
      return STATUS_SUCCESS;
    }

    static NTSTATUS DOKAN_CALLBACK Sia_GetVolumeInformation(LPWSTR VolumeNameBuffer, DWORD VolumeNameSize, LPDWORD VolumeSerialNumber, LPDWORD MaximumComponentLength, LPDWORD FileSystemFlags, LPWSTR FileSystemNameBuffer, DWORD FileSystemNameSize, PDOKAN_FILE_INFO dokanFileInfo) {
      UNREFERENCED_PARAMETER(dokanFileInfo);
      wcscpy_s(VolumeNameBuffer, VolumeNameSize, L"SiaDrive");
      *VolumeSerialNumber = 0x5E05140D;
      *MaximumComponentLength = MAX_PATH;
      *FileSystemFlags = FILE_CASE_SENSITIVE_SEARCH | FILE_CASE_PRESERVED_NAMES | FILE_SUPPORTS_REMOTE_STORAGE | FILE_UNICODE_ON_DISK | FILE_PERSISTENT_ACLS;
      wcscpy_s(FileSystemNameBuffer, FileSystemNameSize, L"NTFS");
      return STATUS_SUCCESS;
    }

    static NTSTATUS DOKAN_CALLBACK Sia_ReadFile(LPCWSTR fileName, LPVOID buffer, DWORD bufferLen, LPDWORD readLength, LONGLONG offset, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);

      NTSTATUS ret = STATUS_SUCCESS;
      
      ExecuteHandleReadOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not (CheckAccess(platformData.DesiredAccess, GENERIC_READ) || CheckAccess(platformData.DesiredAccess, FILE_READ_DATA))) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else {
          std::uint64_t bytesRead;
          if (not ReadBytes(siaPath, (char*)buffer, bufferLen, offset, bytesRead)) {
            ret = DokanNtStatusFromWin32(::GetLastError());
          }
          else {
            *readLength = bytesRead;
          }
        }
      }, offset, bufferLen);

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_WriteFile(LPCWSTR fileName, LPCVOID buffer, DWORD bytesToWrite, LPDWORD bytesWritten, LONGLONG offset, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      NTSTATUS ret = STATUS_SUCCESS;

      ExecuteHandleOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not (CheckAccess(platformData.DesiredAccess, GENERIC_WRITE) || CheckAccess(platformData.DesiredAccess, FILE_WRITE_DATA))) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else if (CheckCachePolicy(filePath, bytesToWrite)) {
          LARGE_INTEGER li = { 0 };
          if (ret == STATUS_SUCCESS) {
            if (not ::GetFileSizeEx(handle, &li)) {
              ret = DokanNtStatusFromWin32(::GetLastError());
            }
            else {
              LARGE_INTEGER distanceToMove;
              if (dokanFileInfo->WriteToEndOfFile) {
                LARGE_INTEGER z = { 0 };
                if (not ::SetFilePointerEx(handle, z, nullptr, FILE_END)) {
                  ret = DokanNtStatusFromWin32(::GetLastError());
                }
              }
              else {
                // Paging IO cannot write after allocate file size.
                if (dokanFileInfo->PagingIo) {
                  if (offset >= li.QuadPart) {
                    *bytesWritten = 0;
                  }
                  else if ((offset + bytesToWrite) > li.QuadPart) {
                    auto bytes = li.QuadPart - offset;
                    if (bytes >> 32) {
                      bytesToWrite = static_cast<DWORD>(bytes & 0xFFFFFFFFUL);
                    }
                    else {
                      bytesToWrite = static_cast<DWORD>(bytes);
                    }
                  }
                }

                if (ret == STATUS_SUCCESS) {
                  if (offset > li.QuadPart) {
                    // In the mirror sample helperZeroFileData is not necessary. NTFS will
                    // zero a hole.
                    // But if user's file system is different from NTFS( or other Windows's
                    // file systems ) then  users will have to zero the hole themselves.
                  }
                  distanceToMove.QuadPart = offset;
                  if (not ::SetFilePointerEx(handle, distanceToMove, nullptr, FILE_BEGIN)) {
                    ret = DokanNtStatusFromWin32(::GetLastError());
                  }
                }
              }

              if (ret == STATUS_SUCCESS) {
                if (::WriteFile(handle, buffer, bytesToWrite, bytesWritten, nullptr)) {
                  fileChanged = true;
                }
                else {
                  ret = DokanNtStatusFromWin32(::GetLastError());
                }
              }
            }
          }
        }
        else {
          ret = DokanNtStatusFromWin32(ERROR_DISK_FULL);
        }
      });

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_SetEndOfFile(LPCWSTR fileName, LONGLONG byteOffset, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      NTSTATUS ret = STATUS_SUCCESS;

      ExecuteHandleOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not CheckAccess(platformData.DesiredAccess, GENERIC_WRITE)) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else {
          LARGE_INTEGER sz = { 0 };
          ::GetFileSizeEx(handle, &sz);

          LARGE_INTEGER offset;
          offset.QuadPart = byteOffset;
          if (not ::SetFilePointerEx(handle, offset, nullptr, FILE_BEGIN)) {
            ret = DokanNtStatusFromWin32(::GetLastError());
          }
          else if (not ::SetEndOfFile(handle)) {
            ret = DokanNtStatusFromWin32(::GetLastError());
          }
          if ((ret == STATUS_SUCCESS) && (offset.QuadPart != (sz.QuadPart - 1))) {
            fileChanged = true;
          }
        }
      });
      
      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static void DOKAN_CALLBACK Sia_CloseFile(LPCWSTR fileName, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      if (dokanFileInfo->Context) {
        auto key = dokanFileInfo->Context;
        dokanFileInfo->Context = 0;
        RemoveOpenFile(siaPath, filePath, key);
      }
    }

    static void DOKAN_CALLBACK Sia_Cleanup(LPCWSTR fileName, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);

      auto key = dokanFileInfo->Context;
      dokanFileInfo->Context = 0;

      RemoveOpenFile(siaPath, filePath, key);

      if (dokanFileInfo->DeleteOnClose) {
        if (dokanFileInfo->IsDirectory) {
          RetryAction([&]() -> BOOL {
            return filePath.DeleteAsDirectory() ? TRUE : FALSE;
          }, DEFAULT_RETRY_COUNT, DEFAULT_RETRY_DELAY_MS)
          && ApiSuccess(GetUploadManager().DirectoryRemoved(siaPath, filePath));
        } else {
          if (ApiSuccess(GetUploadManager().Remove(siaPath))) {
            FilePath::RetryDeleteFileIfExists(filePath);
          }
        }
      }
    }

    static NTSTATUS DOKAN_CALLBACK Sia_FlushFileBuffers(LPCWSTR fileName, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      NTSTATUS ret = STATUS_SUCCESS; 

      ExecuteHandleOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not CheckAccess(platformData.DesiredAccess, GENERIC_WRITE)) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else {
          if (not ::FlushFileBuffers(handle)) {
            ret = DokanNtStatusFromWin32(::GetLastError());
          }
        }
      }, false);

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_DeleteDirectory(LPCWSTR fileName, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath = FilePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      NTSTATUS ret = STATUS_SUCCESS;
      if (dokanFileInfo->DeleteOnClose) {
        filePath.Append("*");
        WIN32_FIND_DATA findData;
        HANDLE findHandle = ::FindFirstFile(&filePath[0], &findData);
        if (findHandle == INVALID_HANDLE_VALUE) {
          ret = DokanNtStatusFromWin32(::GetLastError());
        } else {
          do {
            if ((strcmp(findData.cFileName, "..") != 0) && (strcmp(findData.cFileName, ".") != 0)) {
              ret = STATUS_DIRECTORY_NOT_EMPTY;
            }
          } while ((ret == STATUS_SUCCESS) && (::FindNextFile(findHandle, &findData) != 0));
          DWORD error = ::GetLastError();
          ::FindClose(findHandle);

          if ((ret != STATUS_DIRECTORY_NOT_EMPTY) && (error != ERROR_NO_MORE_FILES)) {
            ret = DokanNtStatusFromWin32(error);
          } else if (ret == STATUS_SUCCESS) {
            auto siaFileTree = GetFileTree();
            if (siaFileTree->DirectoryExists(siaPath)) {
              ret = STATUS_DIRECTORY_NOT_EMPTY;
            }
          }
        }
      }

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_DeleteFile(LPCWSTR fileName, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      NTSTATUS ret = STATUS_SUCCESS;

      ExecuteHandleOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not CheckAccess(platformData.DesiredAccess, DELETE)) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else {
          DWORD dwAttrib = ::GetFileAttributes(&filePath[0]);
          if ((dwAttrib != INVALID_FILE_ATTRIBUTES) && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY)) {
            ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
          }
          else {
            FILE_DISPOSITION_INFO fdi{};
            fdi.DeleteFile = dokanFileInfo->DeleteOnClose;
            if (not ::SetFileInformationByHandle(handle, FileDispositionInfo, &fdi, sizeof(FILE_DISPOSITION_INFO))) {
              ret = DokanNtStatusFromWin32(::GetLastError());
            }
          }
        }
      }, false);

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_MoveFile(LPCWSTR fileName, LPCWSTR newFileName, BOOL ReplaceIfExisting, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      FilePath newFilePath(GetCacheLocation(), newFileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      SString newSiaPath = CSiaApi::FormatToSiaPath(newFileName);
      NTSTATUS ret = STATUS_SUCCESS;
      
      ExecuteHandleOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not CheckAccess(platformData.DesiredAccess, DELETE)) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else {
          size_t len = strlen(&newFilePath[0]);
          auto bufferSize = static_cast<DWORD>(sizeof(FILE_RENAME_INFO) + (len * sizeof(newFilePath[0])));
          auto* renameInfo = static_cast<PFILE_RENAME_INFO>(malloc(bufferSize));
          if (renameInfo) {
            if (dokanFileInfo->IsDirectory) {
              if (not ApiSuccess(GetUploadManager().RenameFolder(siaPath, newSiaPath))) {
                ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
              }
            }
            else if (not ApiSuccess(GetUploadManager().RenameFile(siaPath, newSiaPath))) {
              ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
            }
            if (ret == STATUS_SUCCESS) {
              ::ZeroMemory(renameInfo, bufferSize);
              renameInfo->ReplaceIfExists = ReplaceIfExisting ? TRUE : FALSE; // some warning about converting BOOL to BOOLEAN
              renameInfo->RootDirectory = nullptr; // hope it is never needed, shouldn't be
              renameInfo->FileNameLength = static_cast<DWORD>(len) * sizeof(newFilePath[0]); // they want length in bytes

              wcscpy_s(renameInfo->FileName, len + 1, SString::FromUtf8(&newFilePath[0]).c_str());
              BOOL result = ::SetFileInformationByHandle(handle, FileRenameInfo, renameInfo, bufferSize);
              if (not result) {
                ret = DokanNtStatusFromWin32(::GetLastError());
              }
            }
            free(renameInfo);

            // re-enable uploads
            GetUploadManager().SetRenameActive(false);
          }
          else {
            ret = STATUS_BUFFER_OVERFLOW;
          }
        }
      });

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_SetFileAttributes(LPCWSTR fileName, DWORD fileAttributes, PDOKAN_FILE_INFO dokanFileInfo) {
      UNREFERENCED_PARAMETER(dokanFileInfo);
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);

      NTSTATUS ret = STATUS_SUCCESS;
      RequiredHandleOperation(siaPath, filePath, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not (CheckAccess(platformData.DesiredAccess, FILE_WRITE_ATTRIBUTES) || CheckAccess(platformData.DesiredAccess, GENERIC_WRITE))) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else {
          FILE_BASIC_INFO bi{};
          if (::GetFileInformationByHandleEx(handle, FileBasicInfo, &bi, sizeof(bi))) {
            bi.FileAttributes = fileAttributes;

            if (not ::SetFileInformationByHandle(handle, FileBasicInfo, &bi, sizeof(bi))) {
              ret = DokanNtStatusFromWin32(::GetLastError());
            }
          }
          else {
            ret = DokanNtStatusFromWin32(::GetLastError());
          }
        }
      }, false);
      
      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_GetFileSecurity(LPCWSTR fileName, PSECURITY_INFORMATION securityInfo, PSECURITY_DESCRIPTOR securityDescriptor, ULONG bufferLen, PULONG lengthNeeded, PDOKAN_FILE_INFO dokanFileInfo) {
      UNREFERENCED_PARAMETER(dokanFileInfo);
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);

      NTSTATUS ret = STATUS_SUCCESS;
      const auto  requestingSaclInfo = ((*securityInfo & SACL_SECURITY_INFORMATION) ||
        (*securityInfo & BACKUP_SECURITY_INFORMATION));

      const auto hasSeSecurityNamePriv = AddRemovePrivilege(SE_SECURITY_NAME, TRUE);
      if (not hasSeSecurityNamePriv) {
        *securityInfo &= ~SACL_SECURITY_INFORMATION;
        *securityInfo &= ~BACKUP_SECURITY_INFORMATION;
      }

      HANDLE handle = ::CreateFile(&filePath[0], READ_CONTROL | ((requestingSaclInfo && hasSeSecurityNamePriv) ? ACCESS_SYSTEM_SECURITY: 0),
        FILE_SHARE_READ,
        nullptr,
        OPEN_EXISTING,
        FILE_FLAG_BACKUP_SEMANTICS,
        nullptr);

      if (not handle || (handle == INVALID_HANDLE_VALUE)) {
        ret = DokanNtStatusFromWin32(::GetLastError());
      }
      else {
        if (not ::GetUserObjectSecurity(handle, securityInfo, securityDescriptor, bufferLen, lengthNeeded)) {
          const auto error = ::GetLastError();
          if (error == ERROR_INSUFFICIENT_BUFFER) {
            ret = STATUS_BUFFER_OVERFLOW;
          } else {
            ret = DokanNtStatusFromWin32(error);
          }
        }

        ::CloseHandle(handle);
        if (ret == STATUS_SUCCESS) {
          *lengthNeeded = ::GetSecurityDescriptorLength(securityDescriptor);
        }
      }

      if (hasSeSecurityNamePriv) {
        AddRemovePrivilege(SE_SECURITY_NAME, FALSE);
      }
        
      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_SetFileSecurity(LPCWSTR fileName, PSECURITY_INFORMATION securityInfo, PSECURITY_DESCRIPTOR securityDescriptor, ULONG securityDescriptorLength, PDOKAN_FILE_INFO dokanFileInfo) {
      UNREFERENCED_PARAMETER(securityDescriptorLength);
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      NTSTATUS ret = STATUS_SUCCESS;

      ExecuteHandleOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        LPSTR removePriv = nullptr;
        if (not CheckAccess(platformData.DesiredAccess, WRITE_OWNER) && (*securityInfo == OWNER_SECURITY_INFORMATION)) {
          if (AddRemovePrivilege(SE_TAKE_OWNERSHIP_NAME, TRUE)) {
            removePriv = SE_TAKE_OWNERSHIP_NAME;
          }
        }
        else if (*securityInfo == SACL_SECURITY_INFORMATION) {
          if (not CheckAccess(platformData.DesiredAccess, ACCESS_SYSTEM_SECURITY)) {
            ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
          }
        }

        if (ret == STATUS_SUCCESS) {
          if (not ::SetUserObjectSecurity(handle, securityInfo, securityDescriptor)) {
            ret = DokanNtStatusFromWin32(::GetLastError());
          }
        }

        if (removePriv) {
          AddRemovePrivilege(removePriv, FALSE);
        }
      }, false);

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_SetFileTime(LPCWSTR fileName, CONST FILETIME *creationTime, CONST FILETIME *lastAccessTime, CONST FILETIME *lastWriteTime, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      NTSTATUS ret = STATUS_SUCCESS;

      ExecuteHandleOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not (CheckAccess(platformData.DesiredAccess, FILE_WRITE_ATTRIBUTES) || CheckAccess(platformData.DesiredAccess, GENERIC_WRITE))) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else {
          if (not ::SetFileTime(handle, creationTime, lastAccessTime, lastWriteTime)) {
            ret = DokanNtStatusFromWin32(::GetLastError());
          }
        }
      }, false);

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

    static NTSTATUS DOKAN_CALLBACK Sia_SetAllocationSize(LPCWSTR fileName, LONGLONG allocSize, PDOKAN_FILE_INFO dokanFileInfo) {
      FilePath filePath(GetCacheLocation(), fileName);
      SString siaPath = CSiaApi::FormatToSiaPath(fileName);
      bool changed = false;
      NTSTATUS ret = STATUS_SUCCESS;

      ExecuteHandleOperation(siaPath, filePath, dokanFileInfo->Context, [&](HANDLE handle, bool& fileChanged, const DokanFileConfig& platformData) {
        if (not CheckAccess(platformData.DesiredAccess, GENERIC_WRITE)) {
          ret = DokanNtStatusFromWin32(ERROR_ACCESS_DENIED);
        }
        else {
          LARGE_INTEGER fileSize = { 0 };
          if (::GetFileSizeEx(handle, &fileSize)) {
            if (allocSize < fileSize.QuadPart) {
              LARGE_INTEGER newFileSize{};
              newFileSize.QuadPart = allocSize;
              if (not ::SetFilePointerEx(handle, newFileSize, nullptr, FILE_BEGIN)) {
                ret = DokanNtStatusFromWin32(::GetLastError());
              }
              else if (not ::SetEndOfFile(handle)) {
                ret = DokanNtStatusFromWin32(::GetLastError());
              }
              else {
                changed = true;
              }
            }
          }
          else {
            ret = DokanNtStatusFromWin32(::GetLastError());
          }

          if ((ret == STATUS_SUCCESS) && changed) {
            fileChanged = true;
          }
        }
      });

      return DOKANY_EVENT(__FUNCTION__, ret, siaPath, filePath);
    }

  public:
    static void Initialize(std::shared_ptr<CSiaApi> siaApi, IUploadManager *uploadManager) {
      OSFileOps fileOps;
      fileOps.CheckSharedAccess = [](const DokanFileConfig& firstOpen, const DokanFileConfig& currentOpen)->bool {
        const auto currentAccess = currentOpen.DesiredAccess & (GENERIC_READ | GENERIC_WRITE);
        bool ret = true;
        if ((currentAccess & GENERIC_READ) || (currentAccess & GENERIC_WRITE)) {
          // References:  https://msdn.microsoft.com/en-us/library/windows/desktop/aa363874(v=vs.85).aspx
          //              https://msdn.microsoft.com/en-us/library/windows/desktop/aa363858(v=vs.85).aspx
          ret = false;
          if (((firstOpen.ShareMode & FILE_SHARE_DELETE) != FILE_SHARE_DELETE) && (currentOpen.ShareMode & FILE_SHARE_DELETE)) {
            ret = false;
          }
          else {
            const auto firstShared = firstOpen.ShareMode & (FILE_SHARE_READ | FILE_SHARE_WRITE);
            const auto currentShared = currentOpen.ShareMode & (FILE_SHARE_READ | FILE_SHARE_WRITE);
            if ((firstOpen.DesiredAccess & (GENERIC_READ | GENERIC_WRITE)) == (GENERIC_READ | GENERIC_WRITE)) {
              if ((firstShared == (FILE_SHARE_READ | FILE_SHARE_WRITE))) {
                ret = ((currentAccess == GENERIC_READ) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE))) ||
                  ((currentAccess == GENERIC_WRITE) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE))) ||
                  ((currentAccess == (GENERIC_READ | GENERIC_WRITE)) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE)));
              }
              else if (firstShared == FILE_SHARE_READ) {
                ret = (currentAccess == GENERIC_READ) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE));
              }
              else if (firstShared == FILE_SHARE_WRITE) {
                ret = (currentAccess == GENERIC_WRITE) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE));
              }
            }
            else if (firstOpen.DesiredAccess & GENERIC_READ) {
              if (firstShared == (FILE_SHARE_READ | FILE_SHARE_WRITE)) {
                ret = ((currentAccess == GENERIC_READ) && (currentShared == FILE_SHARE_READ)) ||
                  ((currentAccess == GENERIC_READ) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE))) ||
                  ((currentAccess == GENERIC_WRITE) && (currentShared == FILE_SHARE_READ)) ||
                  ((currentAccess == GENERIC_WRITE) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE))) ||
                  ((currentAccess == (GENERIC_READ | GENERIC_WRITE)) && (currentShared == FILE_SHARE_READ)) ||
                  ((currentAccess == (GENERIC_READ | GENERIC_WRITE)) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE)));
              }
              else if (firstShared == FILE_SHARE_READ) {
                ret = (currentAccess == GENERIC_READ) && ((currentShared == FILE_SHARE_READ) || (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE)));
              }
              else if (firstShared == FILE_SHARE_WRITE) {
                ret = (currentAccess == GENERIC_WRITE) && ((currentShared == FILE_SHARE_READ) || (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE)));
              }
            }
            else if (firstOpen.DesiredAccess & GENERIC_WRITE) {
              if (firstShared == (FILE_SHARE_READ | FILE_SHARE_WRITE)) {
                ret = ((currentAccess == GENERIC_READ) && (currentShared == FILE_SHARE_WRITE)) ||
                  ((currentAccess == GENERIC_READ) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE))) ||
                  ((currentAccess == GENERIC_WRITE) && (currentShared == FILE_SHARE_WRITE)) ||
                  ((currentAccess == GENERIC_WRITE) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE))) ||
                  ((currentAccess == (GENERIC_READ | GENERIC_WRITE)) && (currentShared == FILE_SHARE_WRITE)) ||
                  ((currentAccess == (GENERIC_READ | GENERIC_WRITE)) && (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE)));
              }
              else if (firstShared == FILE_SHARE_READ) {
                ret = (currentAccess == GENERIC_READ) && ((currentShared == FILE_SHARE_WRITE) || (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE)));
              }
              else if (firstShared == FILE_SHARE_WRITE) {
                ret = (currentAccess == GENERIC_WRITE) && ((currentShared == FILE_SHARE_WRITE) || (currentShared == (FILE_SHARE_READ | FILE_SHARE_WRITE)));
              }
            }
          }
        }

        if (not ret) {
          ::SetLastError(ERROR_ACCESS_DENIED);
        }
        return ret;
      };
      fileOps.CloseFile = [](HANDLE& handle)->bool {
        bool ret = ((handle == INVALID_HANDLE_VALUE) || ::CloseHandle(handle));
        handle = INVALID_HANDLE_VALUE;
        return ret;
      };
      fileOps.CreatePlatformData = [](const SString& siaPath, const FilePath& filePath, FileConfig& fileConfig) {
        DWORD flags = (fileConfig.IsDirectory ? FILE_ATTRIBUTE_DIRECTORY | FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_POSIX_SEMANTICS : FILE_FLAG_RANDOM_ACCESS | FILE_ATTRIBUTE_NORMAL);

        DokanFileConfig dfc{};
        dfc.CreateDisp = OPEN_EXISTING;
        dfc.DesiredAccess = GENERIC_READ | GENERIC_WRITE | STANDARD_RIGHTS_REQUIRED;
        dfc.Flags = flags;
        dfc.ShareMode = FILE_SHARE_READ;
        fileConfig.PlatformData = dfc;
      };
      fileOps.OpenCacheFile = [](HANDLE& handle, const SString& siaPath, const FilePath& filePath, const FileConfig& fileConfig)->bool {
        handle = INVALID_HANDLE_VALUE;
        const DokanFileConfig& dfc = fileConfig.PlatformData;
        if (not IsAllowed(siaPath, filePath) || (dfc.Flags & FILE_FLAG_OVERLAPPED)) {
          ::SetLastError(ERROR_ACCESS_DENIED);
        }
        else {
          if (fileConfig.IsDirectory) {
            BOOL success = TRUE;
            if ((dfc.CreateDisp == CREATE_NEW) || (dfc.CreateDisp == OPEN_ALWAYS)) {
              success = ::CreateDirectory(&filePath[0], dfc.SecurityAttrib.get());
              if (success) {
                GetUploadManager().DirectoryAdded(siaPath, filePath);
              }
            }

            if (success) {
              handle = ::CreateFile(&filePath[0], dfc.DesiredAccess, dfc.ShareMode, dfc.SecurityAttrib.get(), OPEN_EXISTING, dfc.Flags | FILE_FLAG_POSIX_SEMANTICS | FILE_FLAG_BACKUP_SEMANTICS, nullptr);
            }
          }
          else {
            // Open as requested and if successfull, open with full access for cache interaction
            handle = ::CreateFile(&filePath[0], dfc.DesiredAccess, dfc.ShareMode, dfc.SecurityAttrib.get(), dfc.CreateDisp, dfc.Flags, nullptr);
            auto err = ::GetLastError();
            if ((handle != INVALID_HANDLE_VALUE)) {
              ::CloseHandle(handle);

              DWORD flags = FILE_FLAG_RANDOM_ACCESS | FILE_ATTRIBUTE_NORMAL;
              handle = ::CreateFile(&filePath[0], GENERIC_READ | GENERIC_WRITE | STANDARD_RIGHTS_ALL, 0, nullptr, OPEN_EXISTING, flags, nullptr);
              ::SetLastError(err);
            }
          }
        }

        return (handle != INVALID_HANDLE_VALUE);
      };
      fileOps.Flush = [](HANDLE handle) {
        ::FlushFileBuffers(handle);
      };
      fileOps.ReadFileBuffer = [](HANDLE handle, char* buf, const size_t& count, const std::uint64_t& offset, std::uint64_t& bytesRead)->bool {
        LARGE_INTEGER distanceToMove{};
        distanceToMove.QuadPart = offset;
        return ::SetFilePointerEx(handle, distanceToMove, nullptr, FILE_BEGIN) &&
          ::ReadFile(handle, buf, count, (DWORD*)&bytesRead, nullptr); 
      };
      fileOps.SeekFile = [](HANDLE handle, const std::uint64_t& pos)->bool {
        LARGE_INTEGER distanceToMove{};
        distanceToMove.QuadPart = pos;
        return ::SetFilePointerEx(handle, distanceToMove, nullptr, FILE_BEGIN) ? true : false;
      };
      fileOps.TruncateByHandle = [](HANDLE handle, const std::uint64_t& toSize)->bool {
        LARGE_INTEGER distanceToMove{};
        distanceToMove.QuadPart = toSize;
        return ::SetFilePointerEx(handle, distanceToMove, nullptr, FILE_BEGIN) &&
          ::SetEndOfFile(handle);
      };
      fileOps.TruncateByPath = [](const FilePath& filePath, const std::uint64_t& toSize)->bool {
        bool ret = false;
        DWORD flags = FILE_FLAG_RANDOM_ACCESS | FILE_ATTRIBUTE_NORMAL;
        HANDLE handle = ::CreateFile(&filePath[0], GENERIC_READ | GENERIC_WRITE | STANDARD_RIGHTS_ALL, 0, nullptr, OPEN_EXISTING, flags, nullptr);
        if (handle != INVALID_HANDLE_VALUE) {
          LARGE_INTEGER distanceToMove{};
          distanceToMove.QuadPart = toSize;
          ret = ::SetFilePointerEx(handle, distanceToMove, nullptr, FILE_BEGIN) &&
            ::SetEndOfFile(handle);
          ::CloseHandle(handle);
        }

        return ret;
      };
      fileOps.ValidateHandle = [](HANDLE handle) {
        return (handle != INVALID_HANDLE_VALUE);
      };
      fileOps.WriteFileBuffer = [](HANDLE handle, const char* buf, const size_t& count, const std::uint64_t& offset, std::uint64_t& bytesWritten)->bool {
        bytesWritten = 0;

        LARGE_INTEGER fs{};
        LARGE_INTEGER distanceToMove{};
        distanceToMove.QuadPart = 0;
        BOOL ret = ::SetFilePointerEx(handle, distanceToMove, &fs, FILE_END);
        if (ret) {
          if (offset != fs.QuadPart) {
            LARGE_INTEGER distanceToMove{};
            distanceToMove.QuadPart = offset;
            ret = ::SetFilePointerEx(handle, distanceToMove, nullptr, FILE_BEGIN);
          }

          if (ret) {
            ret = ::WriteFile(handle, buf, count, (DWORD*)&bytesWritten, nullptr);
          }
        }

        return (ret);
      };
      Configure(siaApi, uploadManager, fileOps);

      _dokanOps.Cleanup = Sia_Cleanup;
      _dokanOps.CloseFile = Sia_CloseFile;
      _dokanOps.DeleteDirectory = Sia_DeleteDirectory;
      _dokanOps.DeleteFile = Sia_DeleteFile;
      _dokanOps.FindFiles = Sia_FindFiles;
      _dokanOps.FindFilesWithPattern = nullptr;
      _dokanOps.FindStreams = nullptr;
      _dokanOps.FlushFileBuffers = Sia_FlushFileBuffers;
      _dokanOps.GetDiskFreeSpace = Sia_GetDiskFreeSpace;
      _dokanOps.GetFileInformation = Sia_GetFileInformation;
      _dokanOps.GetFileSecurity = Sia_GetFileSecurity;
      _dokanOps.GetVolumeInformation = Sia_GetVolumeInformation;
      _dokanOps.LockFile = nullptr;
      _dokanOps.Mounted = Sia_Mounted;
      _dokanOps.MoveFile = Sia_MoveFile;
      _dokanOps.ReadFile = Sia_ReadFile;
      _dokanOps.SetAllocationSize = Sia_SetAllocationSize;
      _dokanOps.SetEndOfFile = Sia_SetEndOfFile;
      _dokanOps.SetFileAttributes = Sia_SetFileAttributes;
      _dokanOps.SetFileSecurity = Sia_SetFileSecurity;
      _dokanOps.SetFileTime = Sia_SetFileTime;
      _dokanOps.UnlockFile = nullptr;
      _dokanOps.Unmounted = Sia_Unmounted;
      _dokanOps.WriteFile = Sia_WriteFile;
      _dokanOps.ZwCreateFile = Sia_ZwCreateFile;
      ::ZeroMemory(&_dokanOptions, sizeof(DOKAN_OPTIONS));
      _dokanOptions.Version = DOKAN_VERSION;
      _dokanOptions.ThreadCount = 0; // use default
      _dokanOptions.Timeout = (60 * 1000) * 60;
      _dokanOptions.Options = DOKAN_OPTION_REMOVABLE;
#ifdef _DEBUG
      _dokanOptions.Options |= DOKAN_OPTION_DEBUG;
#endif
    }

    static void Mount(const wchar_t &driveLetter) {
      if (IsConfigured() && not _mountThread) {
        wchar_t tmp[] = {driveLetter,
                         ':',
                         '\\',
                         0};
        _mountPoint = tmp;
        _mountThread = std::make_unique<std::thread>([&, tmp]() {
          while (GetUploadManager().GetDatabaseRequiresUpgrade()) {
            std::this_thread::sleep_for(1s);
          }
          _dokanOptions.MountPoint = &tmp[0];
          _mountStatus = DokanMain(&_dokanOptions, &_dokanOps);
          CEventSystem::EventSystem.NotifyEvent(CreateMountEndedEvent(_mountPoint, _mountStatus));
        });
      }
    }

    static void NotifyOnline() {
      if (IsConfigured()) {
        GetUploadManager().Start();
      }
    }

    static void NotifyOffline() {
      if (IsConfigured()) {
        GetUploadManager().Stop();
      }
    }

    static void Unmount() {
      if (_mountThread) {
        RetryAction([]() -> BOOL {
          return DokanRemoveMountPoint(SString::FromUtf8(_mountPoint).c_str());
        }, DEFAULT_RETRY_COUNT, DEFAULT_RETRY_DELAY_MS);
        _mountThread->join();
        _mountThread.reset(nullptr);
        _mountPoint = "";
      }
    }

    static void Shutdown() {
      Unmount();
      Release();
      ::ZeroMemory(&_dokanOps, sizeof(_dokanOps));
      ::ZeroMemory(&_dokanOptions, sizeof(_dokanOptions));
    }

    static bool IsMounted() {
      return (_mountThread != nullptr);
    }
};
// Static member variables
DOKAN_OPERATIONS DokanyImpl::_dokanOps;
DOKAN_OPTIONS DokanyImpl::_dokanOptions;
std::unique_ptr<std::thread> DokanyImpl::_mountThread;
NTSTATUS DokanyImpl::_mountStatus = STATUS_SUCCESS;
SString DokanyImpl::_mountPoint;


CDokanyDrive::CDokanyDrive(std::shared_ptr<CSiaApi> siaApi, ITransferManager *transferManager) {
  // Restrict mutex scope
  {
    std::lock_guard<std::mutex> l(_startStopMutex);
    if (DokanyImpl::IsConfigured()) {
      throw SiaDokanDriveException("Sia drive has already been activated");
    }
    DokanyImpl::Initialize(siaApi, uploadManager);
  }
  CEventSystem::EventSystem.AddEventConsumer([this](const CEvent &event) {
    if (event.GetEventName() == "SystemCriticalEvent") {
      Unmount();
    }
  });
}

CDokanyDrive::~CDokanyDrive() {
  CDokanyDrive::Shutdown();
}

bool CDokanyDrive::IsMounted() const {
  return DokanyImpl::IsMounted();
}

void CDokanyDrive::NotifyOnline() {
  DokanyImpl::NotifyOnline();
}

void CDokanyDrive::NotifyOffline() {
  DokanyImpl::NotifyOffline();
}

void CDokanyDrive::Mount(const SString &location) {
  std::lock_guard<std::mutex> l(_startStopMutex);
  DokanyImpl::Mount(location[0]);
}

void CDokanyDrive::Unmount(const bool &clearCache) {
  std::lock_guard<std::mutex> l(_startStopMutex);
  DokanyImpl::Unmount();
}

void CDokanyDrive::ClearCache() {
  std::lock_guard<std::mutex> l(_startStopMutex);
}

void CDokanyDrive::Shutdown() {
  std::lock_guard<std::mutex> l(_startStopMutex);
  DokanyImpl::Shutdown();
}
