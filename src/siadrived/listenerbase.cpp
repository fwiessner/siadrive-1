#include "listenerbase.h"
#include <siadriveconfig.h>
#include <ifaddrs.h>

using namespace Sia::Api;

CListenerBase::CListenerBase(std::shared_ptr<CSiaApi> siaApi, const Protocol& protocol, const ListenerInterface& listenerInterface, const std::uint16_t& port) :
  _siaApi(std::move(siaApi)) {
  if (listenerInterface == ListenerInterface::Private) {
    _listenerList.push_back(std::make_unique<http_listener>(&(ProtocolToString(protocol) + "://localhost" + ":" + SString::FromUInt16(port) + "/api")[0]));
    _listenerList.push_back(std::make_unique<http_listener>(&(ProtocolToString(protocol) + "://127.0.0.1:" + SString::FromUInt16(port) + "/api")[0]));
  } else {
    struct ifaddrs *addrList;
    getifaddrs(&addrList);

    auto* nextAddr = addrList;
    while (nextAddr)
    {
      if (nextAddr->ifa_addr && (nextAddr->ifa_addr->sa_family == AF_INET)) {
        auto *addr = (struct sockaddr_in *)nextAddr->ifa_addr;
        const SString ip = inet_ntoa(addr->sin_addr);
        _listenerList.push_back(std::make_unique<http_listener>(&(ProtocolToString(protocol) + "://" + ip + ":" + SString::FromUInt16(port) + "/api")[0]));
      }

      nextAddr = nextAddr->ifa_next;
    }

    freeifaddrs(addrList);
  }
}

CListenerBase::~CListenerBase() {
  Stop();
}

void CListenerBase::Start() {
  for (auto& listener : _listenerList) {
    auto* p = listener.get();
    p->open().then([this, p]() {
      ConfigureListener(p);
    });
  }
}

void CListenerBase::Stop() {
  for (auto& listener : _listenerList) {
    auto* p = listener.get();
    p->close();
  }
}