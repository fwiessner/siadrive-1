#include "privatelistener.h"
#include <siadriveconfig.h>
#include <platform.h>
#include <siatransfermanager.h>
#include <eventsystem.h>

using namespace Sia::Api;

static const std::int32_t ERROR_DRIVE_MOUNTED = -32000;
extern bool _shutdownRequested;

CPrivateListener::CPrivateListener(std::shared_ptr<CSiaApi> siaApi) :
  _transferManager(new CSiaTransferManager(siaApi)),
  CListenerBase(siaApi, Protocol::Http, ListenerInterface::Private, siaApi->GetSiaDriveConfig()->GetDriveApiPrivatePort()) {
  _siaDrive = std::move(PlatformCreateSiaDrive(siaApi, _transferManager.get()));
  CEventSystem::EventSystem.AddEventConsumer([this](const CEvent& event) {
    if (event.GetEventName() == "SiaOnline") {
      _siaDrive->NotifyOnline();
    } else if (event.GetEventName() == "SiaOffline") {
      _siaDrive->NotifyOffline();
    }
  });
}

CPrivateListener::~CPrivateListener() {
  _siaDrive->Unmount();
  _transferManager->Stop();
  _siaDrive->Shutdown();
  _siaDrive.reset();
  _transferManager.reset();
}

void CPrivateListener::HandleGetMethod(http_request httpRequest, std::unique_ptr<jsonrpcpp::Response>& response) {
  jsonrpcpp::entity_ptr entity = jsonrpcpp::Parser::do_parse(httpRequest.extract_string().get());
  if (entity->is_request()) {
    jsonrpcpp::request_ptr request = std::dynamic_pointer_cast<jsonrpcpp::Request>(entity);

    if (request->method == "mount_status") {
      const auto mounted = _siaDrive->IsMounted();
      response = std::make_unique<jsonrpcpp::Response>(*request, json({
        {"is_mounted", mounted},
        {"location", mounted ? GetSiaDriveConfig().GetLastMountLocation() : ""}
      }));
    }
    else {
      throw jsonrpcpp::MethodNotFoundException(*request);
    }
  }
}

void CPrivateListener::HandlePostMethod(http_request httpRequest, std::unique_ptr<jsonrpcpp::Response>& response) {
  jsonrpcpp::entity_ptr entity = jsonrpcpp::Parser::do_parse(httpRequest.extract_string().get());
  if (entity->is_request()) {
    jsonrpcpp::request_ptr request = std::dynamic_pointer_cast<jsonrpcpp::Request>(entity);

    if (request->method == "mount") {
      std::vector<std::string> args;
      for (const auto& param : request->params.param_array) {
        args.push_back(param.get<std::string>());
      }

      if (_siaDrive->IsMounted()) {
        throw jsonrpcpp::RequestException(jsonrpcpp::Error("Drive already mounted", ERROR_DRIVE_MOUNTED), request->id);
      } else {
        _siaDrive->Mount(args);
        response = std::make_unique<jsonrpcpp::Response>(*request, true);
      }
    } else if (request->method == "unmount") {
      _siaDrive->Unmount();
      response = std::make_unique<jsonrpcpp::Response>(*request, _siaDrive->IsMounted());
    } else if (request->method == "shutdown") {
      _shutdownRequested = true;
      response = std::make_unique<jsonrpcpp::Response>(*request, true);
    }
    else {
      throw jsonrpcpp::MethodNotFoundException(*request);
    }
  }
}

void CPrivateListener::ConfigureListener(http_listener* listener) {
  listener->support(methods::GET, [this](http_request httpRequest) {
    try {
      std::unique_ptr<jsonrpcpp::Response> response;
      HandleGetMethod(httpRequest, response);
      if (response) {
        httpRequest.reply(web::http::status_codes::OK, response->to_json().dump(2));
      }
    } catch (const jsonrpcpp::RequestException &e) {
      httpRequest.reply(web::http::status_codes::BadRequest, e.to_json().dump(2));
    }
  });
  listener->support(methods::POST, [this](http_request httpRequest) {
    try {
      std::unique_ptr<jsonrpcpp::Response> response;
      HandlePostMethod(httpRequest, response);
      if (response) {
        httpRequest.reply(web::http::status_codes::OK, response->to_json().dump(2));
      }
    } catch (const jsonrpcpp::RequestException &e) {
      httpRequest.reply(web::http::status_codes::BadRequest, e.to_json().dump(2));
    }
  });
}