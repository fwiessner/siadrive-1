#include <platform.h>
#include <unistd.h>
#include <sys/stat.h>
#include <siadriveconfig.h>
#include <fstream>
#include <dirent.h>
#include <filepath.h>
#include <sys/file.h>
#include "mac_platform.h"
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/osrng.h>
#include <base64.h>

using namespace Sia::Api;
using namespace CryptoPP;
static int g_instancePid = 0;

static SString Decrypt(char *key, int keyLen, const SString &data) {
  SString ret;
  if (data.Length()) {
    std::vector<BYTE> buffer;
    Base64::Decode((BYTE *) &data[0], data.Length(), buffer);
    CFB_Mode<AES>::Decryption aesDecrypt((const byte *) &key[0], keyLen, (const byte *) &key[0]);
    aesDecrypt.ProcessData((byte *) &buffer[0], (byte *) &buffer[0], buffer.size());
    ret = std::string((char *) &buffer[0], buffer.size());
  }
  return ret;
}

static SString Encrypt(char *key, int keyLen, const SString &data) {
  SString ret;
  if (data.Length()) {
    std::vector<BYTE> encrypted;
    encrypted.resize(data.Length());
    CFB_Mode<AES>::Encryption aesEncrypt((const byte *) &key[0], keyLen, (const byte *) &key[0]);
    aesEncrypt.ProcessData((byte *) &encrypted[0], (byte *) &data[0], encrypted.size());
    std::vector<BYTE> base64data;
    Base64::Encode(&encrypted[0], encrypted.size(), base64data);
    ret = std::string((char *) &base64data[0], base64data.size());
  }
  return ret;
}

bool Sia::Api::PlatformLaunchBundledSiad(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) {
  FilePath dataPath(FilePath::GetAppDataDirectory(), "siadrive");
  dataPath.Append("data");
  dataPath.MakeDirectory();
  return PlatformExecuteProcess(siaDriveConfig, FilePath(FilePath(getResourcesDir(), "sia"), SIAD_EXECUTABLE), dataPath, false);
}

void Sia::Api::PlatformEnableAutoStartOnLogon(const bool &enable) {
  enableAutoStartOnLogon(enable);
}

void Sia::Api::PlatformStoreString(const SString &key, const SString &data) {
  setProtectedData(SString::ToUtf8(key), SString::ToUtf8(data));
}

SString Sia::Api::PlatformRetrieveString(const SString &key) {
  return getProtectedData(key);
}

SString Sia::Api::PlatformSecureDecryptString(const SString &key, const SString &encryptedData) {
  SString ret;
  auto auth = getProtectedData("auth");
  if (auth.size()) {
    std::vector<BYTE> key;
    Base64::Decode((BYTE *) &auth[0], auth.size(), key);
    ret = Decrypt((char *) &key[0], key.size(), encryptedData);
  }
  return ret;
}

SString Sia::Api::PlatformSecureEncryptString(const SString &key, const SString &data) {
  std::vector<BYTE> cryptKey;
  cryptKey.resize(32);
  auto auth = getProtectedData("auth");
  if (not auth.size()) {
    AutoSeededRandomPool rnd;
    SecByteBlock byteBlock(&cryptKey[0], cryptKey.size());
    rnd.GenerateBlock(byteBlock, byteBlock.size());
    std::vector<BYTE> encoded;
    Base64::Encode(&byteBlock[0], byteBlock.size(), encoded);
    setProtectedData("auth", std::string(encoded.begin(), encoded.end()));
  } else {
    Base64::Decode((BYTE *) &auth[0], auth.size(), cryptKey);
  }
  return Encrypt((char *) &cryptKey[0], cryptKey.size(), data);
}

bool Sia::Api::PlatformCheckSingleInstance(const SString &id) {
  g_instancePid = open(&FilePath(FilePath::GetTempDirectory(), id + ".lock")[0], O_CREAT | O_RDWR, 0666);
  const int rc = flock(g_instancePid, LOCK_EX | LOCK_NB);
  bool ret = (rc == 0);
  return ret;
}

void Sia::Api::PlatformReleaseSingleInstance() {
  flock(g_instancePid, LOCK_UN);
}

bool Sia::Api::PlatformExecuteProcess(std::shared_ptr<CSiaDriveConfig> siaDriveConfig, FilePath process, FilePath workingDir, const bool &waitForExit) {
  process.MakeAbsolute();
  workingDir.MakeAbsolute();
  const SString fileName = FilePath(process).StripToFileName();
  SString command = process;
  if (static_cast<SString>(process).EndsWith(SIAD_EXECUTABLE)) {
    SString apiAdr = siaDriveConfig->GetHostNameOrIp() + ":" + SString::FromUInt32(siaDriveConfig->GetApiPort());
    SString hostAdr = ":" + SString::FromUInt32(siaDriveConfig->GetHostPort());
    SString rpcAddr = ":" + SString::FromUInt32(siaDriveConfig->GetRpcPort());
    command = "cd \"" + workingDir + "\" && nohup " + static_cast<SString>(process) + " --sia-directory=\"" + workingDir + "\" --api-addr=" + apiAdr + " --host-addr=" + hostAdr + " --rpc-addr=" + rpcAddr + " &";
  } else {
    command = "cd \"" + workingDir + "\" && nohup " + static_cast<SString>(process) + " &";
  }
  system(&command[0]);
  return true;
}

bool Sia::Api::PlatformIsProcessRunning(const SString &processName) {
  return processIsRunning(processName);
}

bool Sia::Api::PlatformIsProcessRunningExactPath(const FilePath &filePath) {
  SString process = static_cast<SString>(FilePath(filePath).MakeAbsolute());
  return processIsRunningFullPath(process);
}

#ifdef SIADRIVE_PRIVATE_API
#include <siafusedrive.h>

std::unique_ptr<ISiaDrive> Sia::Api::PlatformCreateSiaDrive(std::shared_ptr<CSiaApi> siaApi, ITransferManager *transferManager) {
  return std::unique_ptr<ISiaDrive>(std::make_unique<FUSE::CSiaFuseDrive>(siaApi, transferManager));
}

#endif
