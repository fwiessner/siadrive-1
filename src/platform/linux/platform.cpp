#include <platform.h>
#include <unistd.h>
#include <sys/stat.h>
#include <siadriveconfig.h>
#include <fstream>
#include <dirent.h>
#include <filepath.h>
#include <sys/file.h>
#include <base64.h>
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/osrng.h>

extern "C" {
#include <keyutils.h>
}
using namespace Sia::Api;
using namespace CryptoPP;
static int g_instancePid = 0;

static SString Decrypt(char *key, int keyLen, const SString &data) {
  SString ret;
  if (data.Length()) {
    std::vector<BYTE> buffer;
    Base64::Decode((BYTE *) &data[0], data.Length(), buffer);
    CFB_Mode<AES>::Decryption aesDecrypt((const byte *) &key[0], keyLen, (const byte *) &key[0]);
    aesDecrypt.ProcessData((byte *) &buffer[0], (byte *) &buffer[0], buffer.size());
    ret = std::string((char *) &buffer[0], buffer.size());
  }
  return ret;
}

static SString Encrypt(char *key, int keyLen, const SString &data) {
  SString ret;
  if (data.Length()) {
    std::vector<BYTE> encrypted;
    encrypted.resize(data.Length());
    CFB_Mode<AES>::Encryption aesEncrypt((const byte *) &key[0], keyLen, (const byte *) &key[0]);
    aesEncrypt.ProcessData((byte *) &encrypted[0], (byte *) &data[0], encrypted.size());
    std::vector<BYTE> base64data;
    Base64::Encode(&encrypted[0], encrypted.size(), base64data);
    ret = std::string((char *) &base64data[0], base64data.size());
  }
  return ret;
}

key_serial_t InitKernelKeyring(char *cryptKey, const size_t &cryptKeySize) {
  auto kr = keyctl_get_keyring_ID(KEY_SPEC_USER_KEYRING, 1);
  if (kr == -1) {
    throw ("Keyring auth initialization failed: " + std::to_string(errno));
  }
  {
    auto persistKr = keyctl_get_persistent(getuid(), kr);
    if (persistKr != -1) {
      kr = persistKr;
    }
  }
  if (cryptKey && (cryptKeySize > 0)) {
    auto auth = keyctl_search(kr, "user", "siadrive:auth", 0);
    if (auth == -1) {
      if (errno == ENOKEY) {
        {
          memset(cryptKey, 0, cryptKeySize);
          AutoSeededRandomPool rnd;
          SecByteBlock key((const byte *) &cryptKey[0], cryptKeySize);
          rnd.GenerateBlock(key, key.size());
        }
        if (add_key("user", "siadrive:auth", cryptKey, cryptKeySize, kr) == -1) {
          throw ("Keyring auth read failed: " + std::to_string(errno));
        }
      } else {
        throw ("Keyring auth search failed: " + std::to_string(errno));
      }
    } else {
      auto ret = keyctl_read(auth, cryptKey, cryptKeySize);
      if (ret == -1) {
        throw ("Keyring auth read failed: " + std::to_string(errno));
      }
    }
  }
  return kr;
}

bool Sia::Api::PlatformLaunchBundledSiad(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) {
  FilePath dataPath(FilePath::GetAppDataDirectory(), "siadrive");
  dataPath.Append("data");
  dataPath.MakeDirectory();
  return PlatformExecuteProcess(siaDriveConfig, FilePath("./sia/") + SIAD_EXECUTABLE, dataPath, false);
}

void Sia::Api::PlatformStoreString(const SString &key, const SString &data) {
  auto kr = InitKernelKeyring(nullptr, 0);
  add_key("user", ("siadrive:" + key).str().c_str(), &data[0], data.ByteLength(), kr);
}

SString Sia::Api::PlatformRetrieveString(const SString &key) {
  auto kr = InitKernelKeyring(nullptr, 0);
  SString ret;
  auto dataKey = keyctl_search(kr, "user", ("siadrive:" + key).str().c_str(), 0);
  if (dataKey != -1) {
    char *dataBuff = nullptr;
    auto r = keyctl_read_alloc(dataKey, (void **) &dataBuff);
    if (r != -1) {
      r = keyctl_read(dataKey, &dataBuff[0], r);
      if (r != -1) {
        ret = std::string(dataBuff, 0, r);
      }
      free(dataBuff);
    }
  }
  return ret;
}

SString Sia::Api::PlatformSecureDecryptString(const SString &key, const SString &encryptedData) {
  char cryptKey[32];
  auto kr = InitKernelKeyring(&cryptKey[0], 32);
  SString decrypted = Decrypt(cryptKey, 32, encryptedData);
  return decrypted;
}

SString Sia::Api::PlatformSecureEncryptString(const SString &key, const SString &data) {
  char cryptKey[32];
  auto kr = InitKernelKeyring(&cryptKey[0], 32);
  SString encrypted = Encrypt(cryptKey, 32, data);
  return encrypted;
}

bool Sia::Api::PlatformCheckSingleInstance(const SString &id) {
  g_instancePid = open(&FilePath(FilePath::GetTempDirectory(), id + ".lock")[0], O_CREAT | O_RDWR, 0666);
  const int rc = flock(g_instancePid, LOCK_EX | LOCK_NB);
  bool ret = (rc == 0);
  return ret;
}

void Sia::Api::PlatformReleaseSingleInstance() {
  flock(g_instancePid, LOCK_UN);
}

bool Sia::Api::PlatformExecuteProcess(std::shared_ptr<CSiaDriveConfig> siaDriveConfig, FilePath process, FilePath workingDir, const bool &waitForExit) {
  process.MakeAbsolute();
  workingDir.MakeAbsolute();
  const SString fileName = FilePath(process).StripToFileName();
  SString command = process;
  if (static_cast<SString>(process).EndsWith(SIAD_EXECUTABLE)) {
    SString apiAdr = siaDriveConfig->GetHostNameOrIp() + ":" + SString::FromUInt32(siaDriveConfig->GetSiaApiPort());
    SString hostAdr = ":" + SString::FromUInt32(siaDriveConfig->GetSiaHostPort());
    SString rpcAddr = ":" + SString::FromUInt32(siaDriveConfig->GetSiaRpcPort());
    command = "cd " + workingDir + " && nohup " + static_cast<SString>(process) + " --sia-directory=" + workingDir + " --api-addr=" + apiAdr + " --host-addr=" + hostAdr + " --rpc-addr=" + rpcAddr + " >/dev/null 2>&1 &";
  } else {
    command = "cd " + workingDir + " && nohup " + static_cast<SString>(process) + " >/dev/null 2>&1 &";
  }
  system(&command[0]);
  return true;
}

bool Sia::Api::PlatformIsProcessRunning(const SString &processName) {
  bool exists = false;
  DIR *dir = opendir("/proc");
  if (dir) {
    struct dirent *dp;
    const std::regex r("[1-9]*");
    while (not exists && (dp = readdir(dir))) {
      if ((dp->d_type == DT_DIR) && std::regex_match(dp->d_name, r)) {
        std::ifstream is;
        FilePath src("/proc/", dp->d_name);
        src.Append("comm");
        is.open(&src[0], std::ios::in);
        auto contents = [&is] {
          std::ostringstream ss;
          ss << is.rdbuf();
          return SString(ss.str());
        }();
        exists = (contents == processName);
      }
    }
    closedir(dir);
  }
  return exists;
}

bool Sia::Api::PlatformIsProcessRunningExactPath(const FilePath &filePath) {
  bool exists = false;
  DIR *dir = opendir("/proc");
  if (dir) {
    struct dirent *dp;
    const std::regex r("[1-9]*");
    while (not exists && (dp = readdir(dir))) {
      if ((dp->d_type == DT_DIR) && std::regex_match(dp->d_name, r)) {
        std::ifstream is;
        FilePath src("/proc/", dp->d_name);
        src.Append("cmdline");
        is.open(&src[0], std::ios::in);
        auto contents = [&is] {
          std::ostringstream ss;
          ss << is.rdbuf();
          return SString(ss.str());
        }();
        exists = SString(contents).BeginsWith(FilePath(filePath).MakeAbsolute());
      }
    }
    closedir(dir);
  }
  return exists;
}

#ifdef SIADRIVE_PRIVATE_API
#include <fusedrive.h>

std::unique_ptr<ISiaDrive> Sia::Api::PlatformCreateSiaDrive(std::shared_ptr<CSiaApi> siaApi, ITransferManager *transferManager) {
  return std::unique_ptr<ISiaDrive>(std::make_unique<FUSE::CFuseDrive>(siaApi, transferManager));
}
#endif
