#SiaDrive Dokan API
if (MSVC)
  file(GLOB_RECURSE SIADRIVE_DOKAN_API_SOURCES
    ${CMAKE_CURRENT_SOURCE_DIR}/src/dokany_api/*.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/dokany_api/*.cxx
    ${CMAKE_CURRENT_SOURCE_DIR}/src/dokany_api/*.c
    ${CMAKE_CURRENT_SOURCE_DIR}/include/dokany_api/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/include/dokany_api/*.hpp
    )

  add_library(siadrive.dokany.api SHARED ${SIADRIVE_DOKAN_API_SOURCES})
  CodeSignFile(siadrive.dokany.api siadrive.dokany.api.dll)

  add_dependencies(siadrive.dokany.api siadrive.common.api)
  target_compile_definitions(siadrive.dokany.api PRIVATE SIADRIVE_DOKAN_EXPORT_SYMBOLS=1)
  if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    target_compile_options(siadrive.dokany.api PRIVATE "/MDd")
  else ()
    target_compile_options(siadrive.dokany.api PRIVATE "/MD")
  endif ()
  target_include_directories(siadrive.dokany.api PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include/dokany_api
    )
  target_link_libraries(siadrive.dokany.api siadrive.common.api ${DOKANY_LIBRARIES})

endif ()